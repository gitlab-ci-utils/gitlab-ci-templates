# Changelog

## v38.1.2 (2025-02-28)

- Updated the `socket` job to `socket@0.14.50` to resolve timeout issue.
  See the [release notes](https://github.com/SocketDev/socket-cli-js/releases)
  for details.

### Fixed

## v38.1.1 (2025-02-24)

### Fixed

- Revert `lint_renovate` to v39 as v40 is still pre-release.

## v38.1.0 (2025-02-23)

### Changed

- Added [`check_types`](./jobs/Types-Check.gitlab-ci.yml) job to check types
  with the TypeScript compiler. This can be used with projects using
  TypeScript or JSDoc-based types. The job requires a `tsconfig.json` file to
  specify the `tsc` configuration. (#335)
- Updated the `lint_go` job to `golangci-lint:v1.64.5`. See the
  [release notes](https://github.com/golangci/golangci-lint/releases)
  for details.
  - Also updated the default configuration files in the `lint_go`
    job to v11.3.0. See the
    [release notes](https://gitlab.com/gitlab-ci-utils/config-files/-/releases)
    for details on added, deprecated, and changed analyzers.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@13.3.0`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `lint_renovate` job to `renovate:40`. There were no breaking
  changes impacting GitLab CI Utils projects.
- Updated the `syft_sbom` job to the latest `syft:v1.20.0` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.
- Updated the `.go` template and all Go jobs to the latest
  `golang-1.24.0-alpine3.21` security patch.
- Updated the `.go_test` template to `go-test:2.7.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.

### Fixed

- Updated `node_lts_pnpm_test` job to dynamically update `package.json` with
  packages with allowed lifecycle scripts. The package list can be customized
  via a variable. (#339)
- Updated `node_lts_pnpm_test` job to remove workaround for unmet
  `peerDependencies`, which are now handled like `dependencies`. (#339)
- Fixed OSV Scanner default config file variable overrides in Node collections
  and templates to match Renovate rule.
  - Updated global config for `Node-Version-Tests` collection and
    `Node-Security-Overrides` template to `config-files` `v11.3.0`.
- Updated the `code_count` job to `cloc:1.4.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/cloc/-/releases)
  for details.
- Updated the `remove_image` and `update_annotations` jobs to `image-tools:1.2.4`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/image-tools/-/releases)
  for details.
- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.5.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.
- Updated the `create_release` job to `release-cli:v0.22.0`. See the
  [CHANGELOG](https://gitlab.com/gitlab-org/release-cli/-/blob/master/CHANGELOG.md)
  for details.

## v38.0.0 (2025-02-09)

### Changed

- BREAKING: Updated the `node_sbom` job to `cyclonedx-npm@2.0.0`. See the
  [release notes](https://github.com/CycloneDX/cyclonedx-node-npm/releases)
  for details.
  - Default format updated to CycloneDX v1.6 (was v1.4). This can be changed
    via CLI.
- Updated the `code_count` job to `cloc:1.4.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/cloc/-/releases)
  for details.
- Updated the `.go_test` template to `go-test:2.6.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.

## Fixed

- Updated `node_lts_test` job to expose the code coverage report as "Node Test
  Coverage Report" since various test frameworks are used across projects.
  (#337)
- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:9.1.3`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.5.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.
- Updated the `pa11y_ci` job to `gitlab-pa11y-ci:11.3.3`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@13.2.1`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `lint_prose` job to `vale:3.3.5`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `socket` job to `socket@0.14.43`. See the
  [release notes](https://github.com/SocketDev/socket-cli-js/releases)
  for details.
- Updated the `.go` template and all Go jobs to the latest
  `golang-1.23.6-alpine3.21` security patch.

## Miscellaneous

- Refactored the `lint_powershell` and `powershell-sast` jobs to use the
  [`PSScriptAnalyzer`](https://gitlab.com/gitlab-ci-utils/container-images/psscriptanalyzer)
  image. (#338)

## v37.5.0 (2025-01-26)

### Changed

- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@13.2.0`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `node_sbom` job to `cyclonedx-npm@1.20.0`. See the
  [release notes](https://github.com/CycloneDX/cyclonedx-node-npm/releases)
  for details.
- Updated the `syft_sbom` job to the latest `syft:v1.19.0` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.

### Fixed

- Updated the `remove_image` and `update_annotations` jobs to `image-tools:1.2.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/image-tools/-/releases)
  for details.
- Updated the `prepare_release` job to `gitlab-releaser@8.0.6`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-releaser/-/releases)
  for details.
- Updated the `create_release` job to `release-cli:v0.21.0`. See the
  [CHANGELOG](https://gitlab.com/gitlab-org/release-cli/-/blob/master/CHANGELOG.md)
  for details.
- Updated the `pa11y_ci` job to `gitlab-pa11y-ci:11.3.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases)
  for details.
- Updated the `lint_md` job to `markdownlint-cli2@0.17.2`. See the
  [release notes](https://github.com/DavidAnson/markdownlint-cli2/blob/main/CHANGELOG.md)
  for details.
- Updated the `lint_prose` job to `vale:3.3.4`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `socket` job to `socket@0.14.40`. See the
  [release notes](https://github.com/SocketDev/socket-cli-js/releases)
  for details.
- Updated the `.go` template and all Go jobs to the latest
  `golang-1.23.5-alpine3.21` security patch.
- Updated the `.go_test` template to `go-test:2.5.8`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.
- Updated the `.python` template and jobs to the latest
  `python:3.13.2-alpine3.21` security patch.

## v37.4.0 (2025-01-10)

### Changed

- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.5.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.
- Updated the `pa11y_ci` job to `gitlab-pa11y-ci:11.3.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases)
  for details.
- Updated the `go_sbom` job to `cyclonedx-gomod` v1.9.0. See the
  [release notes](https://github.com/CycloneDX/cyclonedx-gomod/releases)
  for details.
- Updated the `lint_go` job to `golangci-lint:v1.63.4`. See the
  [release notes](https://github.com/golangci/golangci-lint/releases)
  for details.
  - Also updated the default configuration files in the `lint_go`
    job to v11.2.0. See the
    [release notes](https://gitlab.com/gitlab-ci-utils/config-files/-/releases)
    for details on added, deprecated, and changed analyzers.
- Updated the `lint_md` job to `markdownlint-cli2@0.17.1`. See the
  [release notes](https://github.com/DavidAnson/markdownlint-cli2/blob/main/CHANGELOG.md)
  for details.

### Fixed

- Updated the `code_count` job to `cloc:1.3.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/cloc/-/releases)
  for details.
- Updated the `prepare_release` job to `gitlab-releaser@8.0.5`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-releaser/-/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@13.1.1`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `lint_prose` job to `vale:3.3.3`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `socket` job to `socket@0.14.39`. See the
  [release notes](https://github.com/SocketDev/socket-cli-js/releases)
  for details.
- Updated the `lint_nunjucks` job and `.djlint` template to `djlint@1.36.4`. See the
  [release notes](https://github.com/djlint/djLint/releases) for details.
- Updated the `.go` template and all Go jobs to the latest
  `golang-1.23.4-alpine3.21` security patch.
- Updated the `.go_test` template to `go-test:2.5.7`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.
- Updated the `.python` template and jobs to the latest
  `python:3.13.1-alpine3.21` security patch.

## v37.3.1 (2024-12-30)

### Fixed

- Updated `socket` job with the following changes. (#336)
  - Moved to the `socket` package (previously `@socketsecurity/cli`).
  - Updated to `socket@0.14.38`, fixing a dependency issue that caused CLI
    execution to fail (see [this issue](https://github.com/SocketDev/socket-cli/issues/279)).
  - Added a `SOCKET_ENABLED` variable to force the `socket` job to run.
  - Updated triggers for Node child pipelines to include `socket` job.

## v37.3.0 (2024-12-23)

### Changed

- Updated `code_count` job to `cloc:1.3.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/cloc/-/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@13.1.0`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.

### Fixed

- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:9.1.3`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated the `pa11y_ci` job to `gitlab-pa11y-ci:11.2.4`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases)
  for details.
- Updated the `osv_scanner` job to `osv-scanner:v1.9.2`. See the
  [release notes](https://github.com/google/osv-scanner/releases)
  for details.
- Updated the `socket` job to `@socketsecurity/cli@0.14.35`. See the
  [release notes](https://github.com/SocketDev/socket-cli-js/releases)
  for details.
- Updated the `syft_sbom` job to the latest `syft:v1.18.1` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.

## v37.2.0 (2024-12-11)

### Changed

- Updated `GitLab-Security-Scans` collection overrides for changes in GitLab
  17.6, which added Ruby to GitLab Advanced SAST. (#333)
- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:9.1.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated the `update_annotations` job to `image-tools:1.2.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/image-tools/-/releases)
  for details.
- Updated the `lint_md` job to `markdownlint-cli2@0.16.0`. See the
  [release notes](https://github.com/DavidAnson/markdownlint-cli2/blob/main/CHANGELOG.md)
  for details.
- Updated the `lint_prose` job to `vale:3.3.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `syft_sbom` job to the latest `syft:v1.18.0` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.

### Fixed

- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.4.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.
- Updated the `create_release` job to `release-cli:v0.20.0`. See the
  [CHANGELOG](https://gitlab.com/gitlab-org/release-cli/-/blob/master/CHANGELOG.md)
  for details.
- Updated the `pa11y_ci` job to `gitlab-pa11y-ci:11.2.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases)
  for details.
- Updated the `lint_go` job to `golangci-lint:v1.62.2`. See the
  [release notes](https://github.com/golangci/golangci-lint/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@13.0.3`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `socket` job to `@socketsecurity/cli@0.14.33`. See the
  [release notes](https://github.com/SocketDev/socket-cli-js/releases)
  for details.
- Updated the `lint_nunjucks` job and `.djlint` template to `djlint@1.36.3`. See the
  [release notes](https://github.com/djlint/djLint/releases) for details.
- Updated the `.go` template and all Go jobs to Go v1.23.4.
- Updated the `.go_test` template to `go-test:2.5.6`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.
- Updated the `.python` template and jobs to Python 3.13.1.

## v37.1.0 (2024-11-24)

### Changed

- Updated the `pa11y_ci` job to `gitlab-pa11y-ci:11.2.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases)
  for details.
- Updated the `lint_go` job to `golangci-lint:v1.62.0`. See the
  [release notes](https://github.com/golangci/golangci-lint/releases)
  for details.
  - Also updated the default configuration files in the `lint_go`
    job to v11.1.0. See the
    [release notes](https://gitlab.com/gitlab-ci-utils/config-files/-/releases)
    for details on added, deprecated, and changed analyzers.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@13.0.1`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
  - This updated the image to Node 22, which is potentially BREAKING
    for any project that overrides the default CI job template.
- Updated the `lint_powershell` and `powershell-sast` jobs to
  Alpine 3.20 as the base image, which is potentially BREAKING
  for any project that overrides the default CI job template.
- Updated the `syft_sbom` job to the latest `syft:v1.17.0` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.

### Fixed

- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:9.0.3`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated the `lint_prose` job to `vale:3.2.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `socket` job to `@socketsecurity/cli@0.14.27`. See the
  [release notes](https://github.com/SocketDev/socket-cli-js/releases)
  for details.

## v37.0.0 (2024-11-10)

### Changed

- BREAKING: Updated Node LTS `jobs` and `templates` from Node 20 to 22 per the
  [Node release schedule](https://github.com/nodejs/Release). (#331)
- BREAKING: Moved the `container_scanning` job template overrides to file
  [Container-Scanning.gitlab-ci.yml](jobs/Container-Scanning.gitlab-ci.yml),
  which is included in the `Container-Build-Test-Deploy.gitlab-ci.yml`
  collection. Updated these overrides to post-process the vulnerability and
  SBOM reports to convert the temporary image name/tag to the final image
  name/tag. This results in vulnerabilities reported against the final image
  instead of the temporary image, which is deleted in the pipeline. Updated
  the job `rules` to determine the proper final image names, but removed
  `CI_GITLAB_FIPS_MODE` cases since they were not being used and added
  significant complexity to the `rules`. (#280)
- BREAKING: Updated the `pa11y_ci` job to `gitlab-pa11y-ci:11.1.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases)
  for details.
  - Updated base image to Node 22 in v11.0.0.
- Updated `remove_image` job to use the `image-tools` image, which upgrades to
  `regctl` v0.7.2.
- Added [`node_lts_test_macos`](jobs/Node-LTS-Test-MacOS.gitlab-ci.yml) job,
  included in `Node-Version-Tests` collection.
  - Given existing issues with GitLab macOS runners, for example cannot launch
    Chrome, the job is opt-in by setting variable
    `ENABLE_NODE_MACOS_TESTS == 'true'`.
- Updated the `lint_md` job to `markdownlint-cli2@0.15.0`. See the
  [release notes](https://github.com/DavidAnson/markdownlint-cli2/blob/main/CHANGELOG.md)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@12.3.0`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `lint_prose` job to `vale:3.2.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `lint_renovate` job to `renovate:39`. There were no breaking
  changes impacting GitLab CI Utils projects.
- Updated the `syft_sbom` job to the latest `syft:v1.16.0` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.
- Updated the `lint_nunjucks` job and `.djlint` template to `djlint@1.36.1`. See the
  [release notes](https://github.com/djlint/djLint/releases) for details.

### Fixed

- Updated the `osv_scanner` job to `osv-scanner:v1.9.1`. See the
  [release notes](https://github.com/google/osv-scanner/releases)
  for details.
- Updated the `socket` job to `@socketsecurity/cli@0.14.26`. See the
  [release notes](https://github.com/SocketDev/socket-cli-js/releases)
  for details.
- Updated the `update_annotations` job to `image-tools:1.1.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/image-tools/-/releases)
  for details.
- Updated the `.go` template and all Go jobs to Go v1.23.3.
- Updated the `.go_test` template to `go-test:2.5.5`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.

### Miscellaneous

- Added CI job `node_esm_test` to test dedicated ESM project. Changed CI job
  `node_test` to `node_cjs_test` to match updated project name. (#330)

## v36.0.1 (2024-10-26)

### Fixed

- Fixed `node_lts_yarn_test` and `node_lts_yarn_pnp_test` jobs to run
  `test:no-coverage` script, which were missed with #328. (#329)

## v36.0.0 (2024-10-25)

### Changed

- BREAKING: Updated `node_lts_test_win_*` jobs and `Node-Version-Tests`
  collection jobs to run `test:no-coverage` script. All projects will need to
  be updated to add this script if it does not already exist. (#328)
- BREAKING: Updated the `semgrep-sast` job to `gitlab-semgrep-plus:9.0.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- BREAKING: Added job to `Node-Version-Tests` collection to test with Node 23, released
  2024-10-16. (#327)
- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.4.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.
- Updated the `create_release` job to `release-cli:v0.19.0`. See the
  [CHANGELOG](https://gitlab.com/gitlab-org/release-cli/-/blob/master/CHANGELOG.md)
  for details.
- Updated the `pa11y_ci` job to `gitlab-pa11y-ci:10.1.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@12.2.0`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `lint_prose` job to `vale:3.1.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `socket` job to `@socketsecurity/cli@0.14.12`. See the
  [release notes](https://github.com/SocketDev/socket-cli-js/releases)
  for details.
- Updated the `update_annotations` job to `image-tools:1.1.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/image-tools/-/releases)
  for details.

### Fixed

- Refactored the [`tag_version_check`](jobs/Npm-Publish.gitlab-ci.yml) job to
  simplify logic.
- Updated the `syft_sbom` job to the latest `syft:v1.14.2` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.
- Updated the `.go_test` template to `go-test:2.5.4`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.

## v35.1.0 (2024-09-22)

### Changed

- Updated the `lint_powershell` and `powershell-sast` jobs to
  `PSScriptAnalyzer` v1.23.0. See the
  [release notes](https://github.com/PowerShell/PSScriptAnalyzer/releases)
  for details.
- Updated the `osv_scanner` job to `osv-scanner:v1.9.0`. See the
  [release notes](https://github.com/google/osv-scanner/releases)
  for details.
- Updated the `syft_sbom` job to the latest `syft:v1.14.0` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.
- Updated the `.python` template and jobs to Python 3.13.0.

### Fixed

- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:8.6.3`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated `code_count` job to `cloc:1.2.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/cloc/-/releases)
  for details.
- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.3.6`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.
- Updated the `pa11y_ci` job to `gitlab-pa11y-ci:10.0.3`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@12.1.3`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `lint_prose` job to `vale:3.0.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `update_annotations` job to `image-tools:1.0.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/image-tools/-/releases)
  for details.
- Updated the `.go` template and all Go jobs to Go v1.23.2.
- Updated the `.go_test` template to `go-test:2.5.3`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.

## v35.0.0 (2024-09-22)

### Changed

- BREAKING: Enabled
  [GitLab Advanced SAST](https://about.gitlab.com/blog/2024/09/19/gitlab-advanced-sast-is-now-generally-available/)
  in the `GitLab-Security-Scans` collection, which was made generally available
  in GitLab 17.4. This this will only run with a GitLab Ultimate subscription.
  - The Advanced SAST job can be disabled by setting the `variable`
    `GITLAB_ADVANCED_SAST_ENABLED: 'false'`.
  - The `GitLab-Security-Scans` collection currently overrides the default
    `semgrep-sast` job `rules` to run whenever there are file types that it
    can analyze, even when `gitlab-advanced-sast` runs, for comparison of
    results (the default `rules` are set to disable `semgrep-sast`). Note
    this may produce duplicate findings.
- BREAKING: Update `lint_renovate` job to use `--strict` mode to fail if
  Renovate identifies a required, but recoverable, configuration change.
  Currently, Renovate outputs the required change, but executes with the
  change, so it's not obvious it is being flagged.
- Updated `GitLab-Security-Scans` collection overrides for changes in GitLab
  17.4.
- Moved variable `DOCKERFILE` to the global scope in job
  [`container_build`](jobs/Container-Build.gitlab-ci.yml) to allow a single
  override to be used in all container jobs (for example, in the new
  `update_annotations` job).
  - If the variable was previously overridden in the job, it must be moved to
    the global scope to achieve this effect, but even without this the
    `container_build` job will still work as before.
  - Also updated build script `--dockerfile` argument to be a relative path
    instead of a complete path for job consistency.
- Added template
  [`Container-Annotations-Overrides`](./templates/Container-Annotations-Overrides.gitlab-ci.yml).
  This is an override to the `Container-Build-Test-Deploy` collection that adds
  an `update_annotations` job to set the container image OCI annotations and
  add this job as a dependency to subsequent jobs in the collection. As an
  override, this allows projects to opt-in since the new job does have some
  constraints on the project's `Dockerfile`. (#322)
- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:8.6.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated the `go_sbom` job to `cyclonedx-gomod` v1.8.0. See the
  [release notes](https://github.com/CycloneDX/cyclonedx-gomod/releases)
  for details.
- Updated the `lint_go` job to `golangci-lint:v1.61.0`. See the
  [release notes](https://github.com/golangci/golangci-lint/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@12.1.0`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `syft_sbom` job to the latest `syft:v1.12.2` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.

### Fixed

- Updated `container_build` template to pin `kaniko` version (the same as
  previous, v1.23.2), with updates managed by Renovate.
- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.3.5`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.
- Updated the `pa11y_ci` job to `gitlab-pa11y-ci:10.0.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases)
  for details.
- Updated the `osv_scanner` job to `osv-scanner:v1.8.5`. See the
  [release notes](https://github.com/google/osv-scanner/releases)
  for details.
- Updated the `.go_test` template to `go-test:2.5.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.
- Updated the `.python` template and jobs to Python 3.12.6.

### Miscellaneous

- Updated Renovate config to remove `excludeDepNames`, which has been
  deprecated (e.g. from `"excludeDepNames": ["ubuntu"],` to
  `"matchDepNames": ["!ubuntu"]`).

## v34.0.0 (2024-09-08)

### Changed

- BREAKING: Updated the `pa11y_ci` job to `gitlab-pa11y-ci:10.0.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases)
  for details.
- BREAKING: Updated the `lint_prose` job to `vale:3.0.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `lint_md` job to `markdownlint-cli2@0.14.0`. See the
  [release notes](https://github.com/DavidAnson/markdownlint-cli2/blob/main/CHANGELOG.md)
  for details.
- Updated the `socket` job to `@socketsecurity/cli@0.13.0`. See the
  [release notes](https://github.com/SocketDev/socket-cli-js/releases)
  for details.
- Updated the `lint_nunjucks` job and `.djlint` template to `djlint@1.35.2`.

### Fixed

- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:8.5.7`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated `code_count` job to `cloc:1.2.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/cloc/-/releases)
  for details.
- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.3.4`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@12.0.2`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `.go` template and all Go jobs to Go v1.23.1.
- Updated the `.go_test` template to `go-test:2.5.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.

## v33.8.0 (2024-08-27)

### Changed

- Update `osv_scanner` job to allow a global config file to be specified. If
  specified, this is appended to any existing project config file during
  execution (or used alone). See the
  [job definition](jobs/OSV-Scanner.gitlab-ci.yml) for details. (#320)
  - Updated `Npm-Package-Base` collection with a global config file with common
    suppressions.
- Updated `prepare_release`, `secret_detection`, and
  `gemnasium-dependency_scanning` jobs with `needs: []` and removed all
  overrides with those settings in all Collections. (#321)

### Fixed

- Updated the `lint_go` job to `golangci-lint:v1.60.3`. See the
  [release notes](https://github.com/golangci/golangci-lint/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@12.0.1`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details. This updates to Debian Bookworm, which did not impact the jobs,
  but could have impacts if overridden to run other processes.
- Updated the `lint_prose` job to `vale:2.3.3`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `osv_scanner` job to `osv-scanner:v1.8.4`. See the
  [release notes](https://github.com/google/osv-scanner/releases)
  for details.
- Updated the `socket` job to `@socketsecurity/cli@0.11.1`. See the
  [release notes](https://github.com/SocketDev/socket-cli-js/releases)
  for details.

## v33.7.0 (2024-08-21)

### Changed

- Updated `code_count` job to `cloc:1.2.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/cloc/-/releases)
  for details.
- Updated the `lint_go` job to `golangci-lint:v1.60.2`. See the
  [release notes](https://github.com/golangci/golangci-lint/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@12.0.0`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details. This updates to Debian Bookworm, which did not impact the jobs,
  but could have impacts if overridden to run other processes.

### Fixed

- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:8.5.6`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.3.3`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.
- Updated the `pa11y_ci` job to `gitlab-pa11y-ci:9.0.5`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases)
  for details.
- Updated the `lint_prose` job to `vale:2.3.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `syft_sbom` job to the latest `syft:v1.11.1` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.
- Updated the `.go` template and all Go jobs to Go v1.23.0.
- Updated the `.go_test` template to `go-test:2.5.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.

## v33.6.0 (2024-08-11)

### Changed

- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@11.2.0`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `lint_renovate` job to `renovate:38`. There were no breaking
  changes impacting GitLab CI Utils projects.
- Updated the `socket` job to `@socketsecurity/cli@0.11.0`. See the
  [release notes](https://github.com/SocketDev/socket-cli-js/releases)
  for details.
- Updated the `syft_sbom` job to the latest `syft:v1.11.0` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.

### Fixed

- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:8.5.4`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated the `remove_image` job to `regctl` v0.7.1. See the
  [release notes](https://github.com/regclient/regclient/releases) for details.
- Updated the `lint_prose` job to `vale:2.3.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `osv_scanner` job to `osv-scanner:v1.8.3`. See the
  [release notes](https://github.com/google/osv-scanner/releases)
  for details.
- Updated the `.go` template and all Go jobs to Go v1.22.6.
- Updated the `.python` template and jobs to Python 3.12.5.
- Updated the `.go_test` template to `go-test:2.4.3`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.

## v33.5.0 (2024-07-31)

### Changed

- Updated the `lint_prose` job to `vale:2.3.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `syft_sbom` job to the latest `syft:v1.10.0` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.

### Fixed

- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:8.5.3`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated `code_count` job to `cloc:1.1.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/cloc/-/releases)
  for details.
- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.3.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.
- Updated the `socket` job to `@socketsecurity/cli@0.10.1`. See the
  [release notes](https://github.com/SocketDev/socket-cli-js/releases)
  for details.

## v33.4.0 (2024-07-21)

### Changed

- Updated `semgrep-sast` job `rules` overrides for GitLab 17.2. Removed
  `*.html` files as Semgrep support is experimental, and added support for
  `*.cjs` and `*.mjs` files.
- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:8.5.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated the `socket` job to `@socketsecurity/cli@0.10.0`. See the
  [release notes](https://github.com/SocketDev/socket-cli-js/releases)
  for details.

### Fixed

- Added override for GitLab security templates (`container_scanning`,
  `dependency_scanning`, `sast`, `secret_detection`) to keep artifact access
  open to `all` for public pipelines. The default in GitLab 17.2 was made
  'developer`. (#317)
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@11.1.5`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `node_sbom` job to `cyclonedx-npm@1.19.3`. See the
  [release notes](https://github.com/CycloneDX/cyclonedx-node-npm/releases)
  for details.

## v33.3.0 (2024-07-14)

### Changed

- Updated `lint_powershell` and `powershell-sast` to pin current package
  versions (`PSScriptAnalyzer -RequiredVersion 1.22.0` and
  `InjectionHunter -RequiredVersion 1.0.0`). Updated Renovate config to manage
  PowerShell modules installed with `Install-Module`. (#314)
- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:8.4.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated the `syft_sbom` job to the latest `syft:v1.9.0` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.

### Fixed

- Updated GitLab CI job template overrides for changes in GitLab 17.1, which
  added support for C/C++ header files (`.h` and `.hpp`). (#316)
- Removed unused file `/collections/gl-sbom-empty.cdx.json`.
- Updated the `remove_image` job to `regctl` v0.7.0. See the
  [release notes](https://github.com/regclient/regclient/releases) for details.
- Updated the `prepare_release` job to `gitlab-releaser@8.0.4`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-releaser/-/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@11.1.4`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `lint_prose` job to `vale:2.2.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `node_sbom` job to `cyclonedx-npm@1.19.2`. See the
  [release notes](https://github.com/CycloneDX/cyclonedx-node-npm/releases)
  for details.
- Updated the `osv_scanner` job to `osv-scanner:v1.8.2`. See the
  [release notes](https://github.com/google/osv-scanner/releases)
  for details.

## v33.2.0 (2024-07-03)

### Changed

- Updated the `lint_prose` job to `vale:2.2.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.

### Fixed

- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.3.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.
- Updated the `prepare_release` job to `gitlab-releaser@8.0.2`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-releaser/-/releases)
  for details.
- Updated the `pa11y_ci` job to `gitlab-pa11y-ci:9.0.3`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@11.1.3`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `.go` template and all Go jobs to Go v1.22.5.
- Updated the `.go_test` template to `go-test:2.4.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.

## v33.1.0 (2024-06-26)

### Changed

- Updated the `lint_lockfile` job to `lockfile-lint@4.14.0`. See the
  [release notes](https://github.com/lirantal/lockfile-lint/releases)
  for details.
- Updated the `osv_scanner` job to `osv-scanner:v1.8.1`. See the
  [release notes](https://github.com/google/osv-scanner/releases)
  for details.
- Updated the `syft_sbom` job to the latest `syft:v1.8.0` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.

### Fixed

- Updated `go_deploy` job to fail with response codes >= 400. (#315)
- Updated `socket` job with a five minute timeout and one retry. The job
  nominally completes in under 1 minute, but sometimes gets stuck and hangs
  until it times out, which is currently at 60 minutes. In this state it never
  recovers, although typically works when retried. (#313)
- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:8.2.3`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated `code_count` job to `cloc:1.1.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/cloc/-/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@11.1.2`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `lint_prose` job to `vale:2.1.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.

## v33.0.0 (2024-06-13)

### Changed

- BREAKING: Updated the `lint_prose` job to `vale:2.0.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:8.2.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.3.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.
- Updated the `lint_npm_package` job to `npm-package-json-lint@8.0.0`. See the
  [release notes](https://github.com/tclindner/npm-package-json-lint/releases)
  for details.
  - The breaking changes have already been implemented in the templates.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@11.1.0`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `node_sbom` job to `cyclonedx-npm@1.19.0`. See the
  [release notes](https://github.com/CycloneDX/cyclonedx-node-npm/releases)
  for details.
- Updated the `syft_sbom` job to the latest `syft:v1.6.0` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.

### Fixed

- Updated the `prepare_release` job to `gitlab-releaser@8.0.1`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-releaser/-/releases)
  for details.
- Updated the `lint_go` job to `golangci-lint:v1.59.1`. See the
  [release notes](https://github.com/golangci/golangci-lint/releases)
  for details.
- Fixed `create_release` job in tag pipelines to need test pipeline results.
- Updated the `.go` template and all Go jobs to Go v1.22.4.
- Updated the `.go_test` template to `go-test:2.4.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.
- Updated the `.python` template and jobs to Python 3.12.4.

## v32.0.0 (2024-05-30)

### Changed

- BREAKING: Updated `remove_image` job to use
  [`regctl`](https://github.com/regclient/regclient) instead of `reg`,
  resolving issues with deleting images with the latest GitLab container
  registry. (#312).
- Updated the `lint_prose` job to `vale:1.13.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `syft_sbom` job to the latest `syft:v1.5.0` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.

### Fixed

- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:8.1.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@11.0.2`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `osv_scanner` job to `osv-scanner:v1.7.4`. See the
  [release notes](https://github.com/google/osv-scanner/releases)
  for details.

### Miscellaneous

- Updated Renovate config to track `regctl` version. (#312)

## v31.1.0 (2024-05-26)

### Changed

- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:8.1.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated `code_count` job to `cloc:1.1.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/cloc/-/releases)
  for details.
- Updated the `lint_go` job to `golangci-lint:v1.59.0`. See the
  [release notes](https://github.com/golangci/golangci-lint/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@11.0.1`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
  - Note: this is not considered BREAKING since none of the BREAKING changes
    impact the templates or have already been incorporated.
- Updated the `lint_prose` job to `vale:1.12.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `.go` template and all Go jobs to Alpine 3.20.
- Updated the `.go_test` template to `go-test:2.4.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.
- Updated the `.python` template and associated jobs to and Alpine 3.20.

### Fixed

- Updated `remove_image` job in `Container-Build-Test-Deploy` collection to
  allow failure. (#310)
- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.2.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.

### Miscellaneous

- Updated pipeline to trigger pipelines in new
  [Container](https://gitlab.com/gitlab-ci-utils/tests/container-test),
  [Go](https://gitlab.com/gitlab-ci-utils/tests/go-test), and
  [Node](https://gitlab.com/gitlab-ci-utils/tests/node-test) test projects to
  get meaningful test of those templates. This covers the most common jobs
  (but not all). (#311)
- Updated to Renovate presets v1.1.0.

## v31.0.0 (2024-05-09)

### Changed

- BREAKING: Updated GitLab template overrides in the `GitLab-Security-Scans`
  collection for SAST changes in GitLab 17.0. See complete details
  [here](https://docs.gitlab.com/ee/update/deprecations.html?removal_milestone=17.0#sast-analyzer-coverage-changing-in-gitlab-170).
  (#309)
  - Note this is deployed to `gitlab.com`, but GitLab 17.0 will not be released
    officially until 2024-05-16.
- BREAKING: Updated the `semgrep-sast` job to `gitlab-semgrep-plus:8.0.1`,
  including changes for GitLab 17.0. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
  - Note this is deployed to `gitlab.com`, but GitLab 17.0 will not be released
    officially until 2024-05-16.
- BREAKING: Updated the `prepare_release` job to `gitlab-releaser@8.0.0`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-releaser/-/releases)
  for details.
- Updated the `lint_go` job to `golangci-lint:v1.58.0`. See the
  [release notes](https://github.com/golangci/golangci-lint/releases)
  for details.
  - BREAKING: Also updated the default configuration files in the `lint_go`
    job to v10.6.0. See the
    [release notes](https://gitlab.com/gitlab-ci-utils/config-files/-/releases)
    for details on added, deprecated, and changed analyzers.
- Updated the `create_release` job to `release-cli:v0.18.0`. See the
  [CHANGELOG](https://gitlab.com/gitlab-org/release-cli/-/blob/master/CHANGELOG.md)
  for details.
- Updated the `lint_prose` job to `vale:1.11.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `node_sbom` job to `cyclonedx-npm@1.18.0`. See the
  [release notes](https://github.com/CycloneDX/cyclonedx-node-npm/releases)
  for details.
- Updated the `syft_sbom` job to the latest `syft:v1.4.1` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.

### Fixed

- Updated the `osv_scanner` job to `osv-scanner:v1.7.3`. See the
  [release notes](https://github.com/google/osv-scanner/releases)
  for details.
- Updated the `.go` template and all Go jobs to `golang:1.22.3`. See the
  [release notes](https://go.dev/doc/devel/release)
  for details.
- Updated the `.go_test` template to `go-test:2.3.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.

### Miscellaneous

- Fixed issue with Renovate rate limiting MRs per hour.

## v30.0.0 (2024-04-28)

### Changed

- BREAKING: Updated `lint_sh` job to `ubuntu:24.04`. (#308)
- BREAKING: Updated the `Node-Version-Tests` collection to add a `node_22_test`
  job (released 2024-04-25) and removed the `node_21_test` job (end-of-life
  2024-06-01). (#305, #306)
  - Updated the default configuration files in the `lint_npm_package`
    job to v10.5.0 to match the `Node-Version-Tests` collection. See the
    [release notes](https://gitlab.com/gitlab-ci-utils/config-files/-/releases)
    for details.
- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.2.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@10.2.0`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `lint_prose` job to `vale:1.8.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `node_sbom` job to `cyclonedx-npm@1.17.0`. See the
  [release notes](https://github.com/CycloneDX/cyclonedx-node-npm/releases)
  for details.
- Updated the `syft_sbom` job to the latest `syft:v1.3.0` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.

### Fixed

- Updated the `pa11y_ci` job to `gitlab-pa11y-ci:9.0.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases)
  for details.

## v29.6.0 (2024-04-20)

### Changed

- Updated the `create_release` job to `release-cli:v0.17.0`. See the
  [CHANGELOG](https://gitlab.com/gitlab-org/release-cli/-/blob/master/CHANGELOG.md)
  for details.
- Updated the `lint_prose` job to `vale:1.7.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `GitLab-Security-Scans` collection with template changes for GitLab 16.11,
  which adds `*.php` files to the `semgrep-sast` job.

### Fixed

- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@10.1.5`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `osv_scanner` job to `osv-scanner:v1.7.2`. See the
  [release notes](https://github.com/google/osv-scanner/releases)
  for details.
- Updated the `syft_sbom` job to the latest `syft:v1.2.0` image, resolving
  an issue with the `arm64` binary in the `amd64` image. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/syft/-/releases)
  for details.

## v29.5.0 (2024-04-14)

### Changed

- Updated `node_lts_test_win` to use new GitLab Windows 2022 runners, using the
  preconfigured Node release. (#304)
- Updated the `pa11y_ci` job to `gitlab-pa11y-ci:9.0.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases)
  for details. Even though a major release, there were no breaking changes
  for this template.
- Updated the `go_sbom` job to `cyclonedx-gomod` v1.7.0. See the
  [release notes](https://github.com/CycloneDX/cyclonedx-gomod/releases)
  for details.
- Updated the `lint_prose` job to `vale:1.6.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `syft_sbom` job to `syft:v1.2.0`. See the
  [release notes](https://github.com/anchore/syft/releases)
  for details.

### Fixed

- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:6.2.1`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@10.1.4`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `.python` template and jobs to Python 3.12.3.

## v29.4.0 (2024-04-05)

### Changed

- Updated the `lint_md` job to `markdownlint-cli2@0.13.0`. See the
  [release notes](https://github.com/DavidAnson/markdownlint-cli2/blob/main/CHANGELOG.md)
  for details.

### Fixed

- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.1.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.
- Updated the `lint_go` job to `golangci-lint:v1.57.2`. See the
  [release notes](https://github.com/golangci/golangci-lint/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@10.1.2`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `lint_prose` job to `vale:1.5.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `.go` template and all Go jobs to `golang:1.22.2`. See the
  [release notes](https://go.dev/doc/devel/release)
  for details.
- Updated the `.go_test` template to `go-test:2.3.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.
- Updated the `syft_sbom` job to `syft:v1.1.1`. See the
  [release notes](https://github.com/anchore/syft/releases)
  for details.

## v29.3.0 (2024-03-27)

### Changed

- Updated `code_count` job to `cloc` v2.00 using custom image. Also made the
  following changes. (#302)
  - Updated script to output table to console as well as generating JSON file.
  - Added `CLOC_NOT_MATCH_FILES` variable to exclude files from count. Defaults
    to 'package-lock.json`, which was previously excluded.
  - Added `CLOC_CLI_ARGS` variable to specify any additional `cloc` arguments.
- Updated `go_test` `GO_COVER_PACKAGES` to default to `./...`, which now
  includes all packages as of Go 1.22. (#303)
- Added `.go_test_coverage` to `Go-Templates` to simplify adding or overriding
  code coverage related capabilities. Update `go-test` job to use the new
  template. (#303)
- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.1.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@10.1.0`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `lint_prose` job to `vale:1.5.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `syft_sbom` job to `syft:v1.1.0`. See the
  [release notes](https://github.com/anchore/syft/releases)
  for details.
- Updated the `.go_test` template to `go-test:2.3.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.

### Fixed

- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:6.1.2`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.

## v29.2.0 (2024-03-21)

### Changed

- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:6.1.1`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- Updated the `lint_go` job to `golangci-lint:v1.57.1`. See the
  [release notes](https://github.com/golangci/golangci-lint/releases)
  for details. Also updated the default configuration files in the `lint_go`
  job to v10.4.0. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/config-files/-/releases/10.4.0)
  for details.
- Updated the `lint_prose` job to `vale:1.4.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.

### Fixed

- Updated the `pa11y_ci` job to `gitlab-pa11y-ci:8.0.1`.  See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@10.0.2`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.
- Updated the `node_sbom` job to `cyclonedx-npm@1.16.2`.
- Updated the `osv_scanner` job to `osv-scanner:v1.7.1`. See the
  [release notes](https://github.com/google/osv-scanner/releases)
  for details.

## v29.1.0 (2024-03-09)

### Changed

- Updated the `syft_sbom` job to `syft:v1.0.1`. See the
  [release notes](https://github.com/anchore/syft/releases)
  for details.
- Updated the `osv_scanner` job to `osv-scanner:v1.7.0`. See the
  [release notes](https://github.com/google/osv-scanner/releases)
  for details.

### Fixed

- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.0.3`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.
- Updated the `lint_prose` job to `vale:1.3.3`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `socket` job to `@socketsecurity/cli@0.9.3`. See the
  [release notes](https://github.com/SocketDev/socket-cli-js/releases)
  for details.
- Updated the `.go` template and all Go jobs to `golang:1.22.1`. See the
  [release notes](https://go.dev/doc/devel/release)
  for details.
- Updated the `.go_test` template to `go-test:2.2.1`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/go-test/-/releases)
  for details.

## v29.0.0 (2024-02-26)

### Changed

- BREAKING: Updated the `semgrep-sast` job to `gitlab-semgrep-plus:6.0.0`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  for details.
- BREAKING: Updated the `pa11y_ci` job to `gitlab-pa11y-ci:8.0.0`.  See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases)
  for details.
- Updated the `lint_prose` job to `vale:1.2.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases)
  for details.
- Updated the `lint_yaml` job to `yamllint==1.35.1`. See the
  [release notes](https://github.com/adrienverge/yamllint/blob/master/CHANGELOG.rst)
  for details.
- Updated the `syft_sbom` job to `syft:v0.105.1`. See the
  [release notes](https://github.com/anchore/syft/releases)
  for details.

### Fixed

- Updated the `duplication_*` jobs to `gitlab-pmd-cpd:2.0.2`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  for details.
- Updated the `lint_go` job to `golangci-lint:v1.56.2`. See the
  [release notes](https://github.com/golangci/golangci-lint/releases)
  for details.
- Updated the `lint_lockfile` job to `lockfile-lint@4.13.2`. See the
  [release notes](https://github.com/lirantal/lockfile-lint/releases)
  for details.
- Updated the `pagean` and `lint_pageanrc` jobs to `pagean@10.0.1`. See
  the [release notes](https://gitlab.com/gitlab-ci-utils/pagean/-/releases)
  for details.

### Miscellaneous

- Updated the Renovate config to use use new
  [presets](https://gitlab.com/gitlab-ci-utils/renovate-config) project. (#301)

## v28.1.0 (2024-02-11)

### Changed

- Updated the `lint_go` job to `golangci-lint:v1.56.1`.
- Updated the default configuration files in the `lint_go` job to v10.2.0. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/config-files/-/releases/10.2.0)
  for details.
- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:5.2.1`.
- Updated the `lint_lockfile` job to `lockfile-lint@4.13.1`.
- Updated the `lint_yaml` job to `yamllint==1.34.0`.
- Updated the `syft_sbom` job to `syft:v0.104.0`.
- Updated the `.go` template and all Go jobs to `golang:1.22.0`.
- Updated the `.go_test` template to `go-test:2.2.0`.

### Fixed

- Updated the `lint_renovate` job to pull image from `ghcr.io`.
- Updated the `prepare_release` job to `gitlab-releaser@7.0.2`.
- Updated the `lint_prose` job to `vale:1.0.6`.
- Updated the `.python` template and jobs to Python 3.12.2.

## v28.0.0 (2024-02-03)

### Changed

- BREAKING: Replaced `docker_dive` job with `container_dive` job, which no
  longer requires Docker-in-Docker. This also pins the `dive` version, which
  is managed by Renovate. (#299)
- BREAKING: Removed all Docker-specific templates, jobs, and collections as
  listed below as there are now non Docker-in-Docker equivalents for all jobs.
  These should be replaced with the `container_*` equivalents. Also
  consolidated the `Container-Base` collection into the
  `Container-Build-Test-Deploy` collection since now only included there.
  (#299)
  - `/templates/Docker-in-Docker.gitlab-ci.yml`
  - `/jobs/Docker-Build.gitlab-ci.yml`
  - `/jobs/Docker-Deploy.gitlab-ci.yml`
  - `/jobs/Docker-Dive.gitlab-ci.yml`
  - `/collections/Container-Base.gitlab-ci.yml`
  - `/collections/Docker-Build-Test-Deploy.gitlab-ci.yml`
- BREAKING: Updated `pagean` and `lint_pageanrc` jobs to `pagean@10.0.0`.
- Updated `lint_go` image to include full version and digest.
- Updated Renovate config to track versions for `go install`ed dependencies.
- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:5.1.0`.
- Updated the `.go_test` template to `go-test:2.1.0`.

### Fixed

- Fixed `go_mod_updates` job to only run if a `go.sum` file exists, not
  `go.mod`.
- Updated the `go_sbom` job to `cyclonedx-gomod` v1.6.0.
- Updated the `lint_go` job to pin to complete version number and digest.
- Updated the `lint_prose` job to [`vale:1.0.5`](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases),
- Updated the `osv_scanner` job to `osv-scanner:v1.6.2`.
- Updated the `syft_sbom` job to `syft:v0.103.1`.

## v27.0.1 (2024-01-23)

### Fixed

- Reverted the `.dind` template to `docker:24.0.7-alpine3.19` due to issues
  running `docker_dive` with Docker v25. (#298)

## v27.0.0 (2024-01-23)

### Changed

- BREAKING: Updated the `semgrep-sast` job to `gitlab-semgrep-plus:5.0.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases/5.0.0)
  for details.
- BREAKING: Updated the `.dind` template to `docker:25.0.0-alpine3.19`.
- Updated the `lint_prose` job to [`vale:1.0.3`](https://gitlab.com/gitlab-ci-utils/container-images/vale/-/releases),
  including updating to `GCIU` package v1.0.2 and `vale` v3.0.5. Also updated
  default config file to [v10.1.1](https://gitlab.com/gitlab-ci-utils/config-files/-/raw/10.1.1/Linters/.vale.ini),
  supressing several rules.
- Updated the `osv_scanner` job to `osv-scanner:v1.6.1`.
- Updated the `syft_sbom` job to `syft:v0.101.1`.

### Fixed

- Updated `duplication_*` jobs to `gitlab-pmd-cpd:2.0.1`.

## v26.1.1 (2024-01-16)

### Fixed

- Fixed issues with `Lint-Npm-Lockfile-Stylelint` template.

## v26.1.0 (2024-01-15)

### Changed

- Added `GO_BUILD_ARGS` variable to `go_build` and `go_build_multi` jobs to
  allow additional arguments to be passed to `go build`. (#294)
- Added variable to `lint_lockfile` job for additional CLI arguments. (#297)
- Added `Lint-Npm-Lockfile-Stylelint` template with standard override for
  package aliases required by Stylelint. (#297)
- Added `lint_prose` job to lint markdown files with Vale. This defaults to
  only checking `README.MD`, but can be changed via variables. Also included
  in `Code-Quality` collection. Job provides a Code Quality report, but does
  not fail for findings. (#295)
- Updated `semgrep-sast` job to `gitlab-semgrep-plus:4.0.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases/4.0.0)
  for details.
- Updated the `syft_sbom` job to `syft:v0.100.0`.

### Fixed

- Updated all GitLab templates to use the `Jobs/` or `Security/` prefix per
  the latest documentation. There are no changes to the templates. (#292)
- Updated the `lint_md` job to `markdownlint-cli2@0.12.1`.
- Updated the `node_sbom` job to `cyclonedx-npm@1.16.1`.
- Updated the `.go` template and all Go jobs to `golang:1.21.6`.
- Updated the `.go_test` template to `go-test:2.0.1`.

## v26.0.0 (2023-12-26)

### Changed

- BREAKING: Updated all `container`/`docker` jobs to remove the container image
  repository (`CI_APPLICATION_REPOSITORY`) and tag (`CI_APPLICATION_TAG`)
  variables from the job definitions. These must now be provided if the
  individual jobs are used, but can be provided globally which avoids duplicate
  overrides. If using the `Container-Build-Test-Deploy` or
  `Docker-Build-Test-Deploy` collections, no changes are required, and the
  variables `TEMP_IMAGE_REPOSITORY` and `TEMP_IMAGE_TAG` are still used. (#181)
  - This includes the following jobs: `container_build`, `.copy_image` (used
    by container `deploy_branch` and `deploy_tag`), `remove_image`,
    `docker_build`, `.deploy_image` (used by docker `deploy_branch` and
    `deploy_tag`), `docker_dive`, and `container_scanning`.
- BREAKING: Update the `.dind` template and all associated jobs to Alpine 3.19.
- Added `Attestation` template to generate attestation files for a single job,
  and `Attestation-All` template to generate attestation files for all jobs in
  a pipeline. See the
  [GitLab docs](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#artifact-attestation)
  for additional details on attestation files. (#193)
- Updated `semgrep-sast` job to `gitlab-semgrep-plus:3.3.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases/3.3.0)
  for details.

### Fixed

- Updated `node_lts_yarn_pnp_test` job to specifically set Yarn v4. (#279)
- Updated `owasp_dependency_check` job to not check for vulnerability updates
  (with the `--noupdate` flag). The image is rebuilt every 4 hours, so it is
  up to date. If it fails to update, it's likely due to an NVD failure, which
  could cause jobs to fail. (#291)
  - Note this was previously only an override in the `GitLab-Security-Scans`
    collection, but is now the default for the job.
- Updated `go_test` job to set `GO_COVER_PACKAGES` to an explicit list of
  packages to calculate coverage. Previously, the variable was set to `./...`,
  which includes packages in the GOPATH in coverage since it is overridden to
  be within the current directory. This is now set in the `before_script` with
  a complete listing of packages in the project. (#290)
- Updated the `syft_sbom` job to `syft:v0.99.0`.
- Updated the `lint_nunjucks` job and `.djlint` template to `djlint@1.34.1`.

### Miscellaneous

- Updated Renovate config to move from `regexManagers` to `customManagers`.
  (#288)
- Updated the project's default branch to `main`. (#289)

## v25.0.0 (2023-12-13)

### Changed

- BREAKING: Update the `.go` template and jobs to Go v1.21.5 and Alpine 3.19.
- BREAKING: Update the `.python` template and jobs to Python 3.12.1 and Alpine
  3.19.
- BREAKING: Updated the `.go_test` template to `go-test:2.0.0`.
- Updated the `node_sbom` job to `cyclonedx-npm@1.16.0`.
- Updated the `osv_scanner` job to `osv-scanner:v1.5.0`.

### Fixed

- Fixed issues with Renovate rule tracking Alpine updates, which was not
  properly handling images with digests. (#287)

## v24.2.0 (2023-12-04)

### Changed

- Updated the `socket` job to `@socketsecurity/cli@0.9.0`.

### Fixed

- Updated the `node_sbom` job to `cyclonedx-npm@1.14.3`.
- Update the `.go` and `.python` templates to the latest image digest.

## v24.1.0 (2023-11-29)

### Changed

- Updated `owasp_dependency_check` job in `GitLab-Security-Scans` collection
  to set `ODC_ADDITIONAL_ARGS="--noupdate"` so the vulnerability database is
  not updated. (#285)
- Update `container_build` to remove `before_script` that created file with
  GitLab container registry credetials since now handled automatically per
  [the docs](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html). (#286)
- Updated `semgrep-sast` job to `gitlab-semgrep-plus:3.2.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases/3.2.0)
  for details.
- Update the default configuration files in the `lint_go`, `lint_md`, and
  `lint_yaml` jobs to v9.2.0. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/config-files/-/releases/9.2.0)
  for details.
- Updated the `lint_md` job to `markdownlint-cli2@0.11.0`. Note this could be
  BREAKING if the `header` rule aliases are used (not the default).

### Fixed

- Updated the `syft_sbom` job to `syft:v0.98.0`.

## v24.0.1 (2023-11-19)

### Fixed

- Fixed error in the `prepare_release` job that didn't update config from
  variable if a `.gitlab/release.json` fiiles was present, but this file is no
  longer supported.

## v24.0.0 (2023-11-19)

### Changed

- BREAKING: Removed `dependencies` from all applicable jobs to avoid conflicts
  with `needs`. This includes the `remove_image`, `npm-publish` and
  `prepare_release` jobs, and the `.deploy_image` and `.skopeo` templates.
  If both are specified, `dependencies` takes priority over which artifacts are
  downloaded. (#283)
  - Updated `Npm-Package-Base` to specify `npm_install` and `prepare_release`
    as `needs` for `npm_publish` and no needs for `prepare_release`.
- BREAKING: Updated `duplication_*` jobs to `gitlab-pmd-cpd:2.0.0`. The
  `duplication_go` job now properly excludes test files using an explicit list
  of files to analyze (created in `before_script`).
- BREAKING: Updated `semgrep-sast` job to `gitlab-semgrep-plus:3.0.0`. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases/3.0.0)
  for details.
- Updated Go jobs with changes found when using in a Go project:  (#281)
  - Updated `go_mod_tidy` job to only run if `go.sum` exists, but also check
    for `go.mod` to give meaningful failure message.
  - Updated `go_test` job to collect coverage from all packages (`./...`). This
    can be configured to check specific packages by overriding the
    `GO_COVER_PACKAGES` variable.
  - Updated `.go_test` template to always save artifacts.
- Updated the `go_sbom` and `syft_sbom` jobs to save an artificial metrics
  report so they are included in the release evidence collection. (#284)

### Fixed

- Updated the `prepare_release` job to `gitlab-releaser@7.0.1` (from `6.0.0`),
  fixing multiple issues with variable expansion. See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-releaser/-/releases)
  for details.
- Updated the `pa11y_ci` job to `gitlab-pa11y-ci:7.4.0`.  See the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases/7.4.0)
  for details.
- Updated the `lint_npm_package` job to `npm-package-json-lint@7.1.0`.
- Updated the `powershell-sast` and `lint_powershell` jobs to `powershell:7.4`.
- Updated the `lint_yaml` job to `yamllint==1.33.0`.
- Updated the `node_sbom` job to `cyclonedx-npm@1.14.2`.
- Updated the `osv_scanner` job to `osv-scanner:v1.4.3`.
- Updated the `syft_sbom` job to `syft:v0.97.1`.
- Updated the `.go` template and all Go jobs to `golang:1.21.4`.
- Updated the `.go_test` template to `go-test:1.0.1`.

## v23.1.0 (2023-10-31)

### Changed

- Added templates for Go projects including the following collections. (#278)
  - `Go-Base`: Base collection for Go projects, which includes `All-Projects`
    (including `Code-Quality`), `Go-Build-Test-Deploy`, and
    `GitLab-Security-Scans` collections. Intended for Go projects.
  - `Go-Build-Test-Deploy`: Build, test and deploy Go binaries. Intended for
    projects that include Go modules as part of a bigger project.
    - Note that this does not include the `Code-Quality` collection.
- Updated `Code-Quality` collection to include `lint_go` and `duplication_go`
  jobs. (#278)
  - Note: this could be BREAKING if used on existing Go projects.
- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:2.0.0`, adding Go
  rules.
- Updated `syft_sbom` job to `syft:v0.94.0`.

### Fixed

- Updated `osv_scanner` job to OSV Scanner v1.4.2.
- Updated the `.dind` template to `docker:24.0.7-alpine3.18`.

## v23.0.0 (2023-10-19)

### Changed

- BREAKING: Replaced `gemnasium-empty_sbom` job with `syft_sbom` job the in
  `GitLab-Security-Scans` collection. This job runs only when Gemnasium does
  not run and always creates an SBOM (`gemnasium-empty_sbom` did not create an
  SBOM in all cases). Also removed the `no_dependency_sbom` job from the
  `GitLab-Security-Scans` collection, which was deprecated in v22.2.0.
  (#272, #273)
- BREAKING: Updated Node LTS `jobs` and `templates` from Node 18 to 20 per the
  [Node release schedule](https://github.com/nodejs/Release). (#275)
- BREAKING: Updated `Node-Build-Test` collection to run the `node_lts_test`
  job from the lockfile, and updated the `Node-Version-Tests` collection to
  add a `node_20_test` job installing the latest dependencies. (#277)
- BREAKING: Updated the `Node-Version-Tests` collection to
  add a `node_21_test` job (released 2023-10-17). (#264)
- Updated `syft_sbom` job to save SBOM as artifact and pin `syft` version.
  (#274)

### Fixed

- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:1.0.3`.
- Updated the `depcheck` job to `depcheck@1.4.7`.

## v22.2.0 (2023-10-07)

### Changed

- Added `gemnasium-empty_sbom` job to `GitLab-Security-Scans` collection with
  rules to only run in cases where Gemnasium does not otherwise run, which
  forces generation of an empty SBOM when there are no dependencies. (#271)
- Added the `syft_sbom` job. (#271)
- Deprecated the `no_dependency_sbom` job. Will be removed in v23.0.0. (#271)

## v22.1.0 (2023-10-05)

### Changed

- Added job `no_dependency_sbom` to the `GitLab-Security-Scans` collection to
  upload an SBOM with no dependencies to satisfy license scanning execution
  policy requirements. Will only run if variable `NO_DEPENDENCIES` is provided.
  (#270)

### Fixed

- Updated `duplication-*` jobs to `gitlab-pmd-cpd:1.1.0`.
- Updated `osv_scanner` job to OSV Scanner v1.4.1.
- Updated `.python` based jobs to use python v3.12.0.

## v22.0.0 (2023-09-27)

### Changed

- BREAKING: Updated the `prepare_release` and `create_release` jobs to
  [`gitlab-releaser@6.0.0`](https://gitlab.com/gitlab-ci-utils/gitlab-releaser/-/releases/6.0.0).
  Updated `create_release` job to `release-cli:v0.16.0`.
- BREAKING: Updated the `lint_renovate` job to `renovate:37`.
- Updated the `npm_publish` job to include
  [package provenance](https://github.blog/2023-04-19-introducing-npm-package-provenance/).
  (#255)

### Fixed

- Updated the `socket` job with default report link, which has been fixed.
  The JSON report is no longer included as an artifact. (#268)
- Updated the `prepare_release` `RELEASE` variable to not `expand` so the `$` do
  not have to be escaped. (#269)
- Updated the `depcheck` job to `depcheck@1.4.6`.
- Updated the `lint_md` job to `markdownlint-cli2@0.10.0`.
- Updated the `lint_nunjucks` job and `.djlint` template to `djlint@1.34.0`.
- Updated the `node_sbom` job to `cyclonedx-npm@1.14.1`.
- Updated the `semgrep-sast` job to `gitlab-semgrep-plus:1.0.2`.
- Updated Renovate config to track GitLab Semgrep Plus image tag.

## v21.0.2 (2023-09-15)

### Fixed

- Updated `Container-Build-Test-Deploy` collection to remove erroneous
  reference to the `lint_container` job.
- Updated `osv_scanner` job to OSV Scanner v1.4.0.

## v21.0.1 (2023-09-09)

### Fixed

- Updated `lint_npm_package` job to `config-files@9.0.1`, fixing an
  issue with the latest script names.

## v21.0.0 (2023-09-09)

### Changed

- BREAKING: Added `Code-Quality` and `Code-Quality-Npm` collections with the
  appropriate code quality related jobs. (#261)
  - `Code-Quality` is included in the `All-Projects` collection.
  - `Code-Quality-Npm` is included in the `Node-Build-Test` collection.
  - Removed the `All-Project-Slim` collection since `All-Projects` (with
    `Code-Quality`), should be included on all pipelines.
  - Removed `lint_container` from container collections.
- BREAKING: Updated `semgrep-sast` job to `gitlab-semgrep-plus:1.0.0`. See
  breaking changes in the
  [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases/1.0.0).
  (#265)
- BREAKING: Updated `lint_npm_package` job to `config-files@9.0.0`, updating
  package rules to deprecate Node 16 (end-of-life as of 2023-09-11). Compatible
  with all current and LTS releases (^18.12.0 || >=20.0.0).
- BREAKING: Updated the `Node-Version-Tests` collection to deprecate Node 16
  tests (end-of-life 2023-09-11). (#263)
- BREAKING: Updated jobs running `npm` scripts to use the standard `type:sub-type`,
  with each element kebab cased, e.g. `lint:js` (previously `lint-js`). (#260)
- BREAKING: Changed `pmd-cpd-*` jobs to `duplication-*` to better reflect the
  job function. (#262)
- BREAKING: Updated language lint jobs with rules to only run by file type.
  (#262)
- BREAKING: Updated npm package lint jobs with rules to only run if
  `package.json` or `package-lock.json` exists. (#262)
- Updated `secret_detection` in `GitLab-Security-Scans` collection to use
  `needs: []`. (#266)
- Updated `sokrates` job to only run on tag pipelines. (#267)

### Fixed

- Fixed `lint_sh` job to check all `*.sh` files in the repository. (#258)
- Updated `lint_md` job to `markdownlint-cli2@0.9.2`.
- Updated `lint_lockfile` job to `lockfile-lint@4.12.1`.
- Updated the `node_sbom` job to `cyclonedx-npm@1.14.0`.
- Updated README with missing jobs.

### Miscellaneous

- Added GitLab security scans to CI pipeline.

## v20.2.1 (2023-08-23)

### Fixed

- Updated the `depcheck` job to `depcheck@1.4.5`.
- Updated the `pa11y_ci` job to `gitlab-pa11y-ci:7.3.1`.
- Updated the `lint-md` job to `markdownlint-cli2@0.9.0`.
- Updated the `node_sbom` job to `cyclonedx-npm@1.13.1`.
- Updated the `socket` job to `@socketsecurity/cli@0.8.2`.

## v20.2.0 (2023-08-14)

### Changed

- Removed `pagean_count` job, which was used temporarily to gather metrics. (#256)

### Fixed

- Updated the `pa11y_ci` job to `gitlab-pa11y-ci:7.3.0`.
- Updated the `socket` job to `@socketsecurity/cli@0.8.0`.

## v20.1.0 (2023-08-05)

### Changed

- Updated the `pmd_cpd_js`/`pmd_cpd_ts` jobs to `gitlab-pmd-cpd:1.0.0`.
- Updated `lint_lockfile` job to `lockfile-lint@4.12.0`.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.32.0`.

### Fixed

- Updated `osv_scanner` job to OSV Scanner v1.3.6.

## v20.0.0 (2023-07-13)

### Changed

- BREAKING: Updated `lint_renovate` to v36.
- BREAKING: Updated `lint_md`, `lint_npm_package`, `lint_nunjucks`, and `lint_yaml` jobs
  to default config files v8.0.0.

### Fixed

- Updated `semgrep-sast` job to `gitlab-semgrep-plus:4.4.0-rules20230710`.
- Updated `lint_npm_package` to `npm-package-json-lint@7.0.0`
- Updated `pagean` and `lint_pageanrc` jobs to `pagean@9.0.0`.
- Updated the `node_sbom` job to `cyclonedx-npm@1.13.0`.

## v19.1.1 (2023-07-04)

### Fixed

- Pinned `lint_renovate` image to v35 until breaking v36 changes are resolved
  (see [this discusssion](https://github.com/renovatebot/renovate/discussions/23146)).

## v19.1.0 (2023-07-01)

### Changed

- Updated `npm_install` job to use a project-level cache to optimize
  performance. (#254)

### Fixed

- Updated `semgrep-sast` job to `gitlab-semgrep-plus:4.3.6-rules20230628`.
- Updated `lint_lockfile` job to `lockfile-lint@4.10.6`.
- Updated `osv_scanner` job to OSV Scanner v1.3.5.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.31.1`.

## v19.0.0 (2023-06-19)

### Changed

- BREAKING: Updated `Container-Build-Test-Deploy` collection to specify
  `needs` for all jobs to optimize execution. (#251)
- BREAKING: Updated `remove_image` job to not use Docker-in-Docker. Renamed
  `Docker-Remove-Image` file to `Container-Remove-Image` for consistency with
  other jobs. (#62)
- BREAKING: Added `powershell-sast` job and included in the
  `Gitlab-Security-Scans` collection. (#244)
- BREAKING: Deleted the `Docker-Build-Test-Deploy-Schedules-Fail` collection
  since the jobs never fails. (#99)
- Updated `Container-Deploy` job to include `Skopeo` dependency.
- Reverted `code_count` job to Alpine since it now has cloc v1.96.
- Update the `lint_powershell` job to pin to a specific PowerShell version.

### Fixed

- Updated the `pmd_cpd_js`/`pmd_cpd_ts` jobs to `gitlab-pmd-cpd:0.6.0`.
- Updated the `node_sbom` job to `cyclonedx-npm@1.12.1`.
- Updated the `socket` job to `@socketsecurity/cli@0.7.2`.
- Updated the `.dind` template to Docker 24 and Alpine 3.18.
- Updated `.python` based jobs to use python v3.11.4.

### Miscellaneous

- Updated Renovate config to track Alpine OS versions in container image tags
  (e.g. `docker:23.0.6-alpine3.17`). Due to limitations in Renovate, the Alpine
  OS updates will only be made if no application version updates are available.
  Container images will only be identified in the following GitLab CI keywords,
  which are the only current uses: `image: <image>`, `image: name: <image>`,
  `services: - <image>`. (#253)

## v18.1.0 (2023-06-12)

### Changed

- Added `types_test` job to run `tsd` for TypeScript type tests. (#250)
- Updated `npm_check` job to run only on schedule pipelines if a Renovate
  config file is found, and do not run on other pipelines. The `allow_failure`
  settings are unchanged. If no Renovate config file is found, the job will
  run as before. Also updated all collections that include the `npm_check`
  job. (#248)
- Updated `Npm-Package-Base` collection `gemnasium-dependency_scanning` job
  with no `needs` to optimize pipeline. (#249)

### Fixed

- Updated `pa11y_ci` job to `gitlab-pa11y-ci:7.2.0`.
- Updated `lint_md` job to `markdownlint-cli2@0.8.1`.
- Updated `osv_scanner` job to OSV Scanner v1.3.4.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.31.0`.

## v18.0.1 (2023-05-26)

### Fixed

- Updated dependency vulnerability scanning metrics reports for release
  evidence collection. The `osv_scanner` job is now kept in release evidence.
  (#247)
- Reverted `lint_nunjucks` job and `.djlint` template to `djlint@1.27.2` due
  to a [formatting issue](https://github.com/Riverside-Healthcare/djLint/issues/670).
  (#246)

## v18.0.0 (2023-05-26)

### Changed

- BREAKING: Removed Node 19 tests from `Node-Version-Tests` collection, which
  went end-of-life 2023-06-01. (#238)
- Added `pmd_cpd_*` jobs to detect duplicate code. Includes `pmd_cpd_js` job
  for JavaScript and `pmd_cpd_ts` job for TypeScript. (#243)
- BREAKING: Replaced GitLab's `code_quality` template with `pmd_cpd_*` jobs in
  `all-projects` collection. (#243)
- BREAKING: Updated dependency vulnerability scanning in collections for
  pipeline efficiency. This leaves the GitLab `dependency_scanning` template
  and `osv_scanner` jobs running on all pipelines, and the
  `owasp_dependency_check` job only running on `schedule` pipelines. (#242)
  - Added `osv_scanner` job to `Gitlab-Security-Scans` collection.
  - Updated `Gitlab-Security-Scans` collection to only run
    `owasp_dependency_check` job on `schedule` pipelines.
  - Updated `Gitlab-Security-Scans-Schedules-Fail` collection to not allow
    failure for `osv_scanner` job on `schedule` pipelines.

### Fixed

- Updated GitLab job template overrides for changes in GitLab v16.0. (#245)
- Updated `lint_lockfile` job to `lockfile-lint@4.10.5`.
- Updated `lint_yaml` job to `yamllint==1.32.0`.
- Updated `node_sbom` job to `cyclonedx-npm@1.12.0`.
- Updated `osv_scanner` job to OSV Scanner v1.3.3.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.29.0`.

## v17.0.1 (2023-05-11)

### Fixed

- Fixed `semgrep-sast` job to remove digest for missing image. (#240)
- Updated `lint_renovate` variables per latest Renovate job. (#241)
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.27.2`.

## v17.0.0 (2023-05-06)

### Changed

- BREAKING: Removed Node 14 tests from `Node-Version-Tests` collection, went
  end-of-life 2023-04-30. (#237)
- Updated `Npm-Package-Base` collection to optimize deploy/release. (#235)
  - Moved `prepare_release` job to the `pre-deploy` stage so failure will block
    deploy. This is usually caused by a malformed CHANGELOG.
  - Updated `create_release` job to `need` both the `prepare_release`
    and `npm_publish` jobs.
- Updated `lint_md` job to replace `markdownlint-cli` with
  [`markdownlint-cli2`](https://www.npmjs.com/package/markdownlint-cli2). (#239)

### Fixed

- Updated `lint_yaml` job to `yamllint==1.31.0`.
- Updated `node_sbom` job to `cyclonedx-npm@1.11.0`.
- Updated `osv_scanner` job to OSV Scanner v1.3.2.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.25.0`.

## v16.3.0 (2023-04-20)

### Changed

- Added Node 20 tests to `Node-Version-Tests` collection. (#236)
- Updated `semgrep-sast` GitLab template to use images from the
  [GitLab Semgrep Plus](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus)
  project that uses the original GitLab Semgrep image, but also includes the
  JavaScript/TypeScript rules from the `semgrep-rules` repository. This is not
  considered breaking since the SAST jobs do not fail the pipeline when
  vulnerabilities are detected. (#234)

### Fixed

- Updated `node_sbom` job to `cyclonedx-npm@1.10.0`.
- Updated `socket` job to `@socketsecurity/cli@0.6.0`.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.23.3`.

## v16.2.0 (2023-04-05)

### Changed

- Updated default config for `lint_yaml` job to `config-files` `v7.0.0`. This
  is considered non-breaking based on use in this project. For complete
  details, see the [changelog](https://gitlab.com/gitlab-ci-utils/config-files/-/releases/7.0.0).

### Fixed

- Updated `node_sbom` job to `cyclonedx-npm@1.9.2`.
- Updated `osv_scanner` job to OSV Scanner v1.3.1.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.19.1`.
- Updated `.python` based jobs to use python v3.11.3.

## v16.1.0 (2023-03-27)

### Changed

- Updated `needs` to be consistent for all jobs with no dependencies. (#231)
- Updated `needs` to be explicit for all analysis jobs in `container` and
  `docker` collections. (#231)

### Fixed

- Updated GitLab job template overrides for changes in GitLab v15.10. (#232)
- Updated `prepare_release` job to `gitlab-releaser@5.0.1`.
- Updated `lint_yaml` job to `yamllint==1.30.0`.

## v16.0.3 (2023-03-19)

### Fixed

- Updated `socket` job rules to only run on merge request pipelines where there
  are dependency changes (i.e. `package-lock.json` changed), unless the
  variable `SOCKET_DISABLED` is set, with no retries.

## v16.0.2 (2023-03-19)

### Fixed

- Added `SOCKET_DISABLED` variable to disable `socket` job.

## v16.0.1 (2023-03-19)

### Fixed

- Updated `socket` job to allow failure and retry on failure. The API rate
  limit, it is frequently hit runing multiple pipelines in parallel.

## v16.0.0 (2023-03-18)

### Changed

- BREAKING: Move from License Scanning to new License Compliance scanner.
  (#228)
- BREAKING: Updated the `All-Projects` and `All-Projects-Slim` collections to
  shorten the default artifact expiration time to 15 days. (#204)
- Added job to create a risk report with the
  [Socket CLI](https://docs.socket.dev/docs/socket-cli). This is also included
  in the `Node-Build-Test` and `Npm-Package-Base` collections. (#229)

### Fixed

- Updated `node_sbom` job to `cyclonedx-npm@1.9.1`.

## v15.3.1 (2023-03-08)

### Fixed

- Updated `pa11y_ci` job to `gitlab-pa11y-ci:7.1.1`.
- Updated `node_sbom` job to `cyclonedx-npm@1.9.0`.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.19.16`.

## v15.3.0 (2023-02-25)

### Changed

- Added `osv_scanner` job to check for vulnerabilities with Google's
  [OSV Scanner](https://github.com/google/osv-scanner/) tool. This was also
  added to the NPM Package Base collection. The job allows failure, so not
  considered breaking, although any previously suppressed vulnerabilities will
  be identified. For details on suppressing, see
  [the docs](https://google.github.io/osv-scanner/configuration/#ignore-vulnerabilities-by-id).
  (#224, #225)
- Updated `code_count` job to use latest versions of `cloc` (v1.96) from the
  project's image. (#213)
- Added `lint_sh` job to lint Linux shell scripts. (#179)

### Fixed

- Updated GitLab job template overrides for changes in GitLab v15.9. (#227)
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.19.15`.

## v15.2.2 (2023-02-19)

### Fixed

- Updated `pagean` and `lint_pageanrc` jobs to `pagean@8.0.4`.

### Miscellaneous

- Update `renovate` config to not pin digests for `node` and `docker`
  images. (#223)

## v15.2.1 (2023-02-17)

### Fixed

- Fixed syntax error in `lint_pageanrc` job. (#222)

## v15.2.0 (2023-02-17)

### Changed

- Added `lint_pageanrc` job to lint a `.pageanrc.json` file. This is available
  as a separate job, and is included with the `pagean` job. (#221)
- Updated `pagean` job to accept a variable `PAGEANRC_FILE` with the Pagean
  config file location (which defaults to `.pageanrc.json`). (#221)
- Updated `node_sbom` job to upload a
  [`cyclonedx` report](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportscyclonedx).
  (#220)

### Fixed

- Updated `pa11y_ci` job to `gitlab-pa11y-ci:7.0.3`.
- Updated `lint_lockfile` job to `lockfile-lint@4.10.1`.
- Updated `node_sbom` job to `cyclonedx-npm@1.8.0`.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.19.14`.
- Updated `.python` based jobs to use python v3.11.2.

## v15.1.0 (2023-02-03)

### Changed

- Updated `prepare_release` job to `gitlab-releaser@5.0.0`. Note this is
  potentially breaking if empty release notes from a CHANGELOG should be
  allowed. See [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-releaser/-/releases/5.0.0).
  This is released as a non-major release since this is the desired template
  behavior.

### Fixed

- Updated `pagean` job to `pagean@8.0.3`.

## v15.0.0 (2023-01-25)

### Changed

- BREAKING: Updated linting logic for web user interface files. (#214)
  - Added `lint_nunjucks` job to `Web-UI-Tests-With-Lint` collection.
  - Updated `lint_html` job to run only if `.html`, `.handlebars`, or `.hbs`
    files exist and run on `schedule` pipelines.
  - Updated `lint_css` job to run only if `*.css`, `*.html`, `*.handlebars`,
    `*.hbs`, or `*.njk` files exist and run on `schedule` pipelines.
- BREAKING: Updated all templates to remove exceptions for `schedule`
  pipelines. The `.all_except_schedule` template is still available. (#216)
- BREAKING: Changed `docker` nomenclature to the more generic `container`
  equivalent for jobs and pipeline stages. Any remaining jobs with `docker`
  in the name specifically require Docker. (#218)
  - Update `docker` stage names to `container` (e.g. `container-build`,
    `container-test`).
  - Renamed `lint_docker` job to `lint_container`, and renamed file to
    `/jobs/Lint-Container.gitlab-ci.yml`.
- Added variable to `owasp_dependency_check` to allow additional CLI arguments
  to be specified. (#217)
- Updated GitLab template overrides for changes in GitLab 15.8, including
  adding Scala to `semgrep` analysis and removing legacy disables for tools
  now covered by `sempgrep`. (#219)

### Fixed

- Removed unnecessary `extends: .all_except_schedules` from `lint_nunjucks`
  job since it is being overrriden by job `rules`. (#215)
- Updated `lint_md` job to `markdownlint-cli@0.33.0`.
- Updated `lint_lockfile` job to `lockfile-lint@4.10.0`.
- Updated `lint_yaml` job to `yamllint==1.29.0`.
- Updated `pagean` job to `pagean@8.0.2`.
- Updated `unicode_bidi_test` job to `anti-trojan-source@1.4.1`.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.19.13`.

## v14.0.3 (2022-12-24)

### Fixed

- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.19.9`.
- Updated `node_sbom` job to `cyclonedx-npm@1.7.2`.
- Updated `pa11y_ci` job to `gitlab-pa11y-ci:7.0.2`.

## v14.0.2 (2022-12-15)

### Fixed

- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.19.8`.
- Updated `node_sbom` job to `cyclonedx-npm@1.7.0`.
- Updated `prepare_release` job to GitLab `release-cli:v0.15.0`

## v14.0.1 (2022-12-10)

### Changed

- Updated `.python` based jobs to use python v3.11.1.

## v14.0.0 (2022-11-29)

### Changed

- BREAKING: Updated default config for `lint_yaml` job to `config-files` `v6.0.0`, which includes some
  [breaking changes](https://gitlab.com/gitlab-ci-utils/config-files/-/releases/6.0.0). (!235)

### Fixed

- Updated `lint_npm_package` to `npm-package-json-lint@6.4.0`
- Updated `node_sbom` job to `cyclonedx-npm@1.6.1`.

## v13.0.2 (2022-11-10)

### Fixed

- Updated `pa11y_ci` job to `gitlab-pa11y-ci:7.0.1`.
- Updated `lint_docker` job to `hadolint:2.12.0`.
- Updated `node_sbom` job to `cyclonedx-npm@1.4.1`.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.19.7`.

## v13.0.1 (2022-11-04)

### Fixed

- Updated `pagean` job to `pagean:8.0.1`.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.19.4`.

## v13.0.0 (2022-11-02)

### Changed

- BREAKING: Updated `pagean` job to `pagean:8.0.0`.
- BREAKING: Updated `pa11y_ci` job to `gitlab-pa11y-ci:7.0.0`.
- BREAKING: Updated `.dind` template to latest Docker 20 images with pinned tags. (#210)

### Fixed

- Updated `release_check` job to clone the full repository to avoid false negative tests where no git tags are found in a shallow clone. (#211)
- Updated `prepare_release` job to `gitlab-releaser@4.0.3`.
- Updated `node_sbom` job to `cyclonedx-npm@1.3.0`.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.19.3`.
- Updated `.python` template to `python:3.11.0`.

## v12.0.0 (2022-10-23)

### Changed

- BREAKING: Moved `node_sbom` job to new [`@cyclonedx/cyclonedx-npm` module](https://www.npmjs.com/package/@cyclonedx/cyclonedx-npm), v1.2.0. See details on the deprecated [@cyclonedx/bom@4.0.0](https://github.com/CycloneDX/cyclonedx-node-module/releases/tag/v4.0.0) package. This changes several defaults and CLI arguments. See [the module documentation](https://www.npmjs.com/package/@cyclonedx/cyclonedx-npm) for details, and [the `node_sbom` job](jobs/Node-Sbom.gitlab-ci.yml) for new variables and configuration. The updated job produces an equivalent SBOM, but per v1.4 of the SBOM spec (now the default, updated from v1.3).
- BREAKING: Updated `Node-Version-Tests` to include tests with Node 19. (#207)
- BREAKING: Updated Node LTS `jobs` and `templates` from Node 16 to 18 per the [Node release schedule](https://github.com/nodejs/Release). (#208)
- BREAKING: Updated overrides to GitLab templates for changes in GitLab 15.5, which includes adding C# and HTML SAST analysis with `semgrep`. (#209)

### Fixed

- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.19.2`.
- Updated `.python` template to `python:3.10.8`.

## v11.2.1 (2022-10-12)

### Fixed

- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.19.0`.
- Updated `lint_lockfile` job to `lockfile-lint@4.9.6`.
- Updated `prepare_release` job to GitLab `release-cli:v0.14.0`

## v11.2.0 (2022-09-30)

### Changed

- Added `node_lts_test_win_11` job using a group-level Windows 11 runner to
  `/jobs/Node-LTS-Test-Win.gitlab-ci.yml`. The two Windows jobs are mutually
  exclusive. By default, the `node_lts_test_win` job will still be run. Set
  `USE_WINDOWS_11_RUNNER` to `true` to run the `node_lts_test_win_11` job
  instead. Note this runner is only available to jobs in this group. (#206)

### Fixed

- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.18.0`.
- Updated `lint_lockfile` job to `lockfile-lint@4.9.5`.

## v11.1.4 (2022-09-25)

### Fixed

- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.17.2`.
- Updated `pa11y_ci` job to `gitlab-pa11y-ci:6.1.1`.

## v11.1.3 (2022-09-16)

### Fixed

- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.15.0`.
- Updated `lint_yaml` job to `yamllint@1.28.0`.

## v11.1.2 (2022-09-11)

### Fixed

- Updated `Web-UI-Tests-With-Lint` collection to specify `npm_install` as `needs` for `lint_css` and `lint_html`. (#203)

## v11.1.1 (2022-09-07)

### Fixed

- Updated `prepare_release` job to `gitlab-releaser@4.0.2`.
- Updated `pa11y_ci` job to `gitlab-pa11y-ci:6.1.0`.
- Updated `lint_md` job to `markdownlint-cli@0.32.2`.
- Updated `node_sbom` job to `@cyclonedx/bom@3.10.6`.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.12.3`.
- Updated `.python` template and all Python jobs to `python:3.10.7-alpine3.16`.

## v11.1.0 (2022-08-12)

### Changed

- Pin `yamllint` version in [`lint_yaml`](./jobs/Lint-Yaml.gitlab-ci.yml) job, and update to use default config file instead of variable. (#198, #200)
- Added [`.python`](./templates/Python.gitlab-ci.yml) template as base for all Python jobs (`.djlint`, `lint_nunjucks`, `lint_yaml`), with the python image as well as standard `pip` settings. (#198, #201)
- Updated `sokrates` job to expose the report artifacts in merge requests and added artifical metrics report to collect release evidence. (#192)

### Fixed

- Updated `lint_lockfile` job to `lockfile-lint@4.8.0`.

## v11.0.0 (2022-08-05)

### Changed

- BREAKING: Updated `pa11y-ci` job to [`gitlab-pa11y-ci` v6.0.0](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases/6.0.0) image.
- BREAKING: Update `pagean` job to [`pagean@7.0.0`](https://gitlab.com/gitlab-ci-utils/pagean/-/releases/7.0.0)
- Added [`lint_nunjucks`](./jobs/Lint-Nunjucks.gitlab-ci.yml) job to lint [Nunjucks files](https://mozilla.github.io/nunjucks/) with `djlint`. (#196)
- Added [`.djlint`](./templates/Djlint.gitlab-ci.yml) template to lint various formats with `djlint`. (#196)

### Fixed

- Updated `prepare_release` job to `gitlab-releaser@4.0.1`
- Updated `lint_md` job to `markdownlint-cli@0.32.1`
- Updated `lint_lockfile` job to `lockfile-lint@4.7.7`.

### Miscellaneous

- Update renovate config to manage `pip` package versions installed in templates. (#196)

## v10.0.0 (2022-07-21)

### Changed

- BREAKING: Updated `npm_check` job to `npm-check@6.0.1`.
- BREAKING: Updated `lint_md` job to `markdownlint-cli@0.32.0` and updated default configurastion with new rules.
- BREAKING: Updated `prepare_release` job to GitLab `release-cli:v0.13.0`

### Fixed

- Updated `lint_lockfile` job to `lockfile-lint@4.7.6`.
- Updated `node_sbom` job to `@cyclonedx/bom@3.10.4`.

## v9.1.0 (2022-06-23)

### Changed

- Added [Sokrates](https://www.sokrates.dev/) job to `all-projects-slim` template to continue to gather data from a variety of projects for evaluation. The job allows failure, so not breaking. (#191)

### Fixed

- Updated GitLab template overrides for changes in GitLab 15.1. (#188)
- Updated `node_sbom` job to `@cyclonedx/bom@3.10.1`

## v9.0.0 (2022-06-17)

### Changed

- BREAKING: Updated `lint_lockfile` config to deprecate Node 12 and 17 support since both are end-of-life. Compatible with all current and LTS releases (^14.15.0 || ^16.13.0 || >=18.0.0).
- BREAKING: Update `lint_docker` job to use Alpine linux-based image. (#189)
- BREAKING: Update `code_count` job metrics to be properly Prometheus-formatted metrics with labels. All labels will show as new in the next pipeline. (#190)
- Updated `lint_lockfile` job to `lockfile-lint@4.7.5`
- Updated `node_sbom` job to `@cyclonedx/bom@3.10.0`
- Updated `prepare_release` job to `gitlab-releaser@4.0.0`

## v8.0.2 (2022-05-25)

### Fixed

- Updated GitLab container scanning predefined templates overrides for changes in GitLab 15.0 (no functional changes). (#187)

## v8.0.1 (2022-05-25)

### Fixed

- Updated GitLab predefined templates overrides for changes in GitLab 15.0 (no functional changes). (#187)

## v8.0.0 (2022-05-22)

### Changed

- BREAKING: Update `deploy_branch` (in `Container-Deploy` and `Docker-Deploy`) and `release_check` jobs to use `$CI_DEFAULT_BRANCH` instead of defaulting to "master" (primarily to make compatible with "main"). This is BREAKING for cases where `$CI_DEFAULT_BRANCH` does not represent the latest published branch (e.g. using GitfLow, where `$CI_DEFAULT_BRANCH` would normally be `develop`, but `master`/`main` is the latest published branch). (#141)
- BREAKING: Removed Node 17 from `Node-Version-Tests` collection since end-of-life as of 2022-06-01. (#178)

## v7.0.0 (2022-05-12)

### Changed

- BREAKING: Updated `lint_md` job to pull default configuration file from `config-files` project rather than create from variable `MARKDOWNLINT_RULES`. This default configuration is updated per the latest `markdownlint` rules. If the `MARKDOWNLINT_RULES` variable is overridden it must be changed to reference a file URL or add a `.markdownlint.json` files to the project. (#184)
- BREAKING: Update file `jobs/Lint-NPM-Package.gitlab-ci.yml` to `jobs/Lint-Npm-Package.gitlab-ci.yml` for consistency. (#184)
- BREAKING: Updated `lint_npm_package` default config after `config-files` project move. (#184)
- BREAKING: Update for `config-files` v3.0.0 at new project location
- Added job running the [Sokrates](https://www.sokrates.dev/) code examination tool. (#185)
- Updated `node_sbom` job to `@cyclonedx/bom@3.9.0`

## v6.0.1 (2022-05-01)

### Fixed

- Updated `lint_npm_package` job to remove Node 12 and warn on `engines` (until all modules transitioned off Node 12). (#183)

## v6.0.0 (2022-05-01)

### Changed

- BREAKING: Removed Node 12 from `Node-Version-Tests` collection since end-of-life as of 2022-04-30. (#176)
- Updated to latest dependencies:
  - `node_sbom` job to `@cyclonedx/bom@3.8.0`
  - `pagean` job to `pagean@6.0.9`

### Fixed

- Updated `pnpm` test to trap install errors to avoid failing for unmet peer dependencies. If there are actual install errors, the tests will fail. (#182)

## v5.0.0 (2022-04-22)

### Changed

- BREAKING: Renamed file `jobs/Npm-Lint-Lockfile.gitlab-ci.yml` to `jobs/Lint-Npm-Lockfile.gitlab-ci.yml` for consistency with other files. Only breaking if included directly, all collections were updated. (#173)
- BREAKING: Added `lint_prettier` job and included in Node-Build-Test collection. (#172)
- BREAKING: Added Node 18 tests to `Node-Version-Tests` collection. (#177)
- Pinned `lint_npm_package` job default configuration file to the current version and updated the renovate config to manage updates. (#174)
- Updated `node_sbom` job to `@cyclonedx/bom` to v3.7.0

### Fixed

- Disabled `lint_npm_package` on schedule pipelines for consistency with other lint jobs (#175)
- Updated GitLab template overrides for GitLab 14.10. (#180)

## v4.2.1 (2022-03-31)

### Fixed

- Fixed GitLab `dependency_scanning` template overrides for v14.9 updates. Artifacts include dependency report for all jobs, and SBOM reports if applicable.
- Fixed `pagean_count` needs (#171)

## v4.2.0 (2022-03-28)

### Changed

- Update to `pagean` v6.0.8, including job to improve install count (#170)

## v4.1.0 (2022-03-27)

### Changed

- Updated `prepare_release` job to `gitlab-releaser` v3.0.0. This is not a breaking change for this template as written. **Note this could be a breaking change if the `script` is overridden. See the [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-releaser/-/releases/3.0.0) for details on the breaking changes in `gitlab-releaser` v3.0.0.**

## v4.0.0 (2022-03-20)

### Changed

- BREAKING: Updated `lint_lockfile` and `unicode_bidi_test` jobs to install dependency in `before_script` and execute in `script` (previously used `npx` in script). Only breaking if `before_script` is overridden. (#169)
- BREAKING: Updated `lint_lockfile` to validate that the resolved URL matches the package name (`--validate-package-names`)

### Fixed

- Updated jobs to latest dependencies:
  - `lint_lockfile`: Update `lockfile-lint` to v4.7.4
  - `lint_npm_package`: Update `npm-package-json-lint` to v6.3.0
  - `node_sbom`: Update `@cyclonedx/bom` to v3.6.0
  - `unicode_bidi_test`: Update `anti-trojan-source` to v1.4.0

## v3.2.1 (2022-03-07)

### Fixed

- Pinned image digests for `create_release`, `pa11y_ci`, `pagean`

## v3.2.0 (2022-03-07)

### Changed

- Update `node_sbom` job to use the preferred call of `cyclonedx-bom` per v3.5.0 (#168)
- Pinned versions for the following jobs to better handle breaking changes. Updates are now managed by `renovate`.(#167)
  - Node packages: `depcheck`, `json_schema_secure`, `lint_lockfile`, `lint_md`, `lint_npm_package`, `node_sbom`, `npm_check`, `prepare_release`, `unicode_bidi_test`
  - Container images: `create_release`, `pa11y_ci`, `pagean`

## v3.1.0 (2022-02-26)

### Changed

- Disabled `retire-js-dependency_scanning` since deprecated. The functionality is covered by both GitLab's Gemnasium and OWASP Dependency Check. (#166)
- Update `secret_detection` to always run a full scan to resolve MR issues and simplify override logic (#165)

## v3.0.0 (2022-01-28)

### Changed

- BREAKING: Update `lint_npm_package` to not allow failure (#159)
- BREAKING: Updated file name for `unicode_bidi_test` to follow convention (#164)
- BREAKING: Update Hadolint job name to `lint_docker` (and file name) to follow convention (#160)
- Add retry for `node_lts_test_win` job (#162)
- Update `create_release` to GitLab release-cli v0.11.0 (#140)

### Fixed

- Update all GitLab template overrides for v14.7 updates (#163)

## v2.3.1 (2021-12-23)

### Fixed

- Updated `lint_npm_package` with variable to specify the path to lint, which defaults to `./package.json`. (#161)

## v2.3.0 (2021-12-21)

### Added

- Add job to lint NPM package with [`npm-package-json-lint`](https://www.npmjs.com/package/npm-package-json-lint). (#157)

### Fixed

- Updated `pa11y_ci` job for `pa11y-ci-reporter-html` v4 using the `pa11y-ci` reporter interface. (#158)

## v2.2.2 (2021-12-11)

### Fixed

- Fixed `unicode_bidi_test` job to run without dependencies

## v2.2.1 (2021-12-11)

### Fixed

- Fixed `unicode_bidi_test` job name for consistency and added documentation on overrides

## v2.2.0 (2021-12-11)

### Changed

- Added `unicode-bidi-test` job to detect trojan source attacks that employ unicode bidirectional character vulnerabilities to inject malicious code. This job is included in `All-Projects-Slim` collection. (#154)

## v2.1.0 (2021-11-30)

### Changed

- Updated `secret_detection` override to run on tags to include analysis in release evidence collection (#152)
- Update Yarn 2 PnP test (`node_lts_yarn_pnp_test`) to save latest `yarn` artifacts on failure (#153)

## v2.0.0 (2021-10-27)

### Changed

- BREAKING: Updated node LTS jobs to v16 since now the active LTS (#149)
- BREAKING: Updated all node jobs to leverage `.node` template, which is pinned to the active version. (#149)
- Updated GitLab job overrides for GitLab 14.4 (#150)

### Miscellaneous

- Added comments to `code_count` job (#151)

## v1.4.0 (2021-10-20)

### Added

- Added Node 17 to `Node-Version-Tests` collection (#148)

### Changed

- Updated `Npm-Package-Base` collection to remove `npm_check` on schedule pipelines since all npm packages are now using `renovate` for dependency updates. (#147)

## v1.3.1 (2021-10-08)

### Changed

- Update `docker_deploy` and `container_deploy` jobs to output source/destination image names to job log. (#145)

## v1.3.0 (2021-10-07)

### Added

- Added `Container-Build-Test-Deploy` collection, with jobs that leverage [kaniko](https://github.com/GoogleContainerTools/kaniko) for container image builds (`Container-Build`) and [skopeo](https://github.com/containers/skopeo) for deploying container images (`Container-Deploy`). Since these tools do not require Docker-in-Docker, there's a noticeable reduction in pipeline times. The legacy `Docker-Build-Test-Deploy` collection is functionally unchanged, other than extracting common sections of both to the `Container-Base` collection. (#142, #143)

## v1.2.1 (2021-09-24)

### Fixed

- Update GitLab template overrides for v14.3 (#139)
- Pin `create_release` job to `release-cli` v0.9.0 until date issue is v0.10.0 is fixed ([details](https://gitlab.com/gitlab-ci-utils/gitlab-releaser/-/issues/33))

## v1.2.0 (2021-09-17)

### Added

- Added job to lint PowerShell using [PSScriptAnalyzer](https://github.com/PowerShell/PSScriptAnalyzer) (#138)

### Changed

- Add JSON report to artifacts from `dependency_check` job (#135)

## v1.1.0 (2021-08-29)

### Added

- Added job to lint renovate config files (`renovate.json` or `.gitlab/renovate/json`), and includes in `all-projects-slim` collection (#133)

## v1.0.1 (2021-08-24)

### Fixed

- Update GitLab template overrides for v14.2 (#131)

## v1.0.0 (2021-08-24)

### Changed

- This project has previously not used versioned releases, but to better manage breaking changes establish baseline versioned release at 1.0.0 (#130)
