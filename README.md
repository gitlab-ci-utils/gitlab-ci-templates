# GitLab CI Templates

## Overview

A collection of CI jobs, job templates, and collections of jobs that can be
included in other CI pipelines.

Each job runs in one of the following standard stages as shown below. Each of
these may have a pre- and/or post- stage as required.

```mermaid
graph LR;
  A(prepare)-->B(lint);
  B-->C(build);
  C-->D(test);
  D-->E(deploy);
  E-->F(container-build);
  F-->G(container-test);
  G-->H(container-deploy);
  H-->I(release);
```

## Jobs

The following are individual job files, grouped by the stage in which they
execute. Job with standard variations by pipeline type (for example default
branches, other branches, tags) are included together in the same job file.
Where applicable, standard GitLab template jobs are included (with no links).
Job files that include multiple integrated jobs that run in multiple stages
are listed in all applicable stages.

- `prepare`
  - [Go-Mod-Download.gitlab-ci.yml](./jobs/Go-Mod-Download.gitlab-ci.yml)
  - [Npm-Install.gitlab-ci.yml](./jobs/Npm-Install.gitlab-ci.yml)
- `lint`
  - [Lint-Container.gitlab-ci.yml](./jobs/Lint-Container.gitlab-ci.yml)
  - [Lint-Go.gitlab-ci.yml](./jobs/Lint-Go.gitlab-ci.yml)
  - [Lint-Md.gitlab-ci.yml](./jobs/Lint-Md.gitlab-ci.yml)
  - [Lint-Npm-Package.gitlab-ci.yml](./jobs/Lint-Npm-Package.gitlab-ci.yml)
  - [Lint-Nunjucks.gitlab-ci.yml](./jobs/Lint-Nunjucks.gitlab-ci.yml)
  - [Lint-Pageanrc.gitlab-ci.yml](./jobs/Lint-Pageanrc.gitlab-ci.yml)
  - [Lint-Powershell.gitlab-ci.yml](./jobs/Lint-Powershell.gitlab-ci.yml)
  - [Lint-Prose.gitlab-ci.yml](./jobs/Lint-Prose.gitlab-ci.yml)
  - [Lint-Renovate.gitlab-ci.yml](./jobs/Lint-Renovate.gitlab-ci.yml)
  - [Lint-Shell.gitlab-ci.yml](./jobs/Lint-Shell.gitlab-ci.yml)
  - [Lint-Yaml.gitlab-ci.yml](./jobs/Lint-Yaml.gitlab-ci.yml)
  - [Npm-Lint-Css.gitlab-ci.yml](./jobs/Npm-Lint-Css.gitlab-ci.yml)
  - [Npm-Lint-Html.gitlab-ci.yml](./jobs/Npm-Lint-Html.gitlab-ci.yml)
  - [Npm-Lint-Lockfile.gitlab-ci.yml](./jobs/Npm-Lint-Lockfile.gitlab-ci.yml)
  - [Npm-Lint-Js.gitlab-ci.yml](./jobs/Npm-Lint-Js.gitlab-ci.yml)
  - [Npm-Lint-Md.gitlab-ci.yml](./jobs/Npm-Lint-Md.gitlab-ci.yml)
  - [Types-Check.gitlab-ci.yml](./jobs/Types-Check.gitlab-ci.yml)
- `build`
  - [Go-Build.gitlab-ci.yml](./jobs/Go-Build.gitlab-ci.yml)
- `test`
  - `dependency_scanning`, `sast`, `secret_detection`
  - [Code-Count.gitlab-ci.yml](./jobs/Code-Count.gitlab-ci.yml)
  - [Depcheck.gitlab-ci.yml](./jobs/Depcheck.gitlab-ci.yml)
  - [Duplication.gitlab-ci.yml](./jobs/Duplication.gitlab-ci.yml)
  - [GitLab-pa11y-ci.gitlab-ci.yml](./jobs/GitLab-pa11y-ci.gitlab-ci.yml)
  - [Go-Mod-Tidy.gitlab-ci.yml](./jobs/Go-Mod-Tidy.gitlab-ci.yml)
  - [Go-Mod-Updates.gitlab-ci.yml](./jobs/Go-Mod-Updates.gitlab-ci.yml)
  - [Go-Sbom.gitlab-ci.yml](./jobs/Go-Sbom.gitlab-ci.yml)
  - [Go-Test-Race.gitlab-ci.yml](./jobs/Go-Test-Race.gitlab-ci.yml)
  - [Go-Test.gitlab-ci.yml](./jobs/Go-Test.gitlab-ci.yml)
  - [Json-Schema-Secure.gitlab-ci.yml](./jobs/Json-Schema-Secure.gitlab-ci.yml)
  - [Node-LTS-Test-MacOS.gitlab-ci.yml](./jobs/Node-LTS-Test-MacOS.gitlab-ci.yml)
  - [Node-LTS-Test-Win.gitlab-ci.yml](./jobs/Node-LTS-Test-Win.gitlab-ci.yml)
  - [Node-LTS-Test.gitlab-ci.yml](./jobs/Node-LTS-Test.gitlab-ci.yml)
  - [Node-Sbom.gitlab-ci.yml](./jobs/Node-Sbom.gitlab-ci.yml)
  - [Npm-Audit.gitlab-ci.yml](./jobs/Npm-Audit.gitlab-ci.yml)
  - [Npm-Check.gitlab-ci.yml](./jobs/Npm-Check.gitlab-ci.yml)
    (and [Npm-Check-Schedules-Fail.gitlab-ci.yml](./jobs/Npm-Check-Schedules-Fail.gitlab-ci.yml))
  - [OSV-Scanner.gitlab-ci.yml](./jobs/OSV-Scanner.gitlab-ci.yml)
  - [OWASP-Dependency-Check.gitlab-ci.yml](./jobs/OWASP-Dependency-Check.gitlab-ci.yml)
  - [Pagean.gitlab-ci.yml](./jobs/Pagean.gitlab-ci.yml)
  - [SAST-Powershell.gitlab-ci.yml](./jobs/SAST-Powershell.gitlab-ci.yml)
  - [Socket.gitlab-ci.yml](./jobs/Socket.gitlab-ci.yml)
  - [Sokrates.gitlab-ci.yml](./jobs/Sokrates.gitlab-ci.yml)
  - [Syft.gitlab-ci.yml](./jobs/Syft.gitlab-ci.yml)
  - [Types-Test.gitlab-ci.yml](./jobs/Types-Test.gitlab-ci.yml)
  - [Unicode-Bidi-Test.gitlab-ci.yml](./jobs/Unicode-Bidi-Test.gitlab-ci.yml)
- `pre-deploy`
  - [Npm-Publish.gitlab-ci.yml](./jobs/Npm-Publish.gitlab-ci.yml)
- `deploy`
  - [Go-Deploy.gitlab-ci.yml](./jobs/Go-Deploy.gitlab-ci.yml)
  - [Npm-Publish.gitlab-ci.yml](./jobs/Npm-Publish.gitlab-ci.yml)
- `container-build`
  - [Container-Build.gitlab-ci.yml](./jobs/Container-Build.gitlab-ci.yml)
- `container-test`
  - [Anchore.gitlab-ci.yml](./jobs/Anchore.gitlab-ci.yml)
  - [Container-Dive.gitlab-ci.yml](./jobs/Container-Dive.gitlab-ci.yml)
  - [Container-Scanning.gitlab-ci.yml](./jobs/Container-Scanning.gitlab-ci.yml)
- `container-deploy`
  - [Container-Deploy.gitlab-ci.yml](./jobs/Container-Deploy.gitlab-ci.yml)
- `pre-release`
  - [GitLab-Releaser.gitlab-ci.yml](./jobs/GitLab-Releaser.gitlab-ci.yml)
- `release`
  - [GitLab-Releaser.gitlab-ci.yml](./jobs/GitLab-Releaser.gitlab-ci.yml)
  - [Release-Check.gitlab-ci.yml](./jobs/Release-Check.gitlab-ci.yml)
  - `pages`
- `.post`
  - [Container-Remove-Image.gitlab-ci.yml](./jobs/Container-Remove-Image.gitlab-ci.yml)

## Collections

The following are collections of the preceding jobs, grouped by project type.

- [All-Projects.gitlab-ci.yml](./collections/All-Projects.gitlab-ci.yml)
- [Code-Quality.gitlab-ci.yml](./collections/Code-Quality.gitlab-ci.yml)
- [Code-Quality-Npm.gitlab-ci.yml](./collections/Code-Quality-Npm.gitlab-ci.yml)
- [Container-Build-Test-Deploy.gitlab-ci.yml](./collections/Container-Build-Test-Deploy.gitlab-ci.yml)
- [GitLab-Security-Scans.gitlab-ci.yml](./collections/GitLab-Security-Scans.gitlab-ci.yml) (includes `Dependency-Scanning`, `License-Management`, `SAST`)
- [Go-Build-Test-Deploy.gitlab-ci.yml](./collections/Go-Build-Test-Deploy.gitlab-ci.yml)
- [Go-Base.gitlab-ci.yml](./collections/Go-Base.gitlab-ci.yml)
- [Node-Build-Test.gitlab-ci.yml](./collections/Node-Build-Test.gitlab-ci.yml)
  (and [Node-Build-Test-Schedules-Fail.gitlab-ci.yml](./collections/Node-Build-Test-Schedules-Fail.gitlab-ci.yml))
- [Node-Version-Tests.gitlab-ci.yml](./collections/Node-Version-Tests.gitlab-ci.yml)
- [Npm-Package-Base.gitlab-ci.yml](./collections/Npm-Package-Base.gitlab-ci.yml)
- [Npm-Publish-Release.gitlab-ci.yml](./collections/Npm-Publish-Release.gitlab-ci.yml)
- [Web-UI-Tests.gitlab-ci.yml](./collections/Web-UI-Tests.gitlab-ci.yml)
  (and [Web-UI-Tests-With-Lint.gitlab-ci.yml](./collections/Web-UI-Tests-With-Lint.gitlab-ci.yml))

## Templates

The following are templates for standard configurations that are extended in
other jobs.

- [Attestation-All.gitlab-ci.yml](./templates/Attestation-All.gitlab-ci.yml)
- [Attestation.gitlab-ci.yml](./templates/Attestation.gitlab-ci.yml)
- [Container-Annotations-Overrides.gitlab-ci.yml](./templates/Container-Annotations-Overrides.gitlab-ci.yml)
- [Djlint.gitlab-ci.yml](./templates/Djlint.gitlab-ci.yml)
- [Global-Templates.gitlab-ci.yml](./templates/Global-Templates.gitlab-ci.yml)
- [Go-Templates.gitlab-ci.yml](./templates/Go-Templates.gitlab-ci.yml)
- [Lint-Npm-Lockfile-Stylelint.gitlab-ci.yml](/templates/Lint-Npm-Lockfile-Stylelint.gitlab-ci.yml)
- [Node-Security-Overrides.gitlab-ci.yml](/templates/Node-Security-Overrides.gitlab-ci.yml)
- [Node-Test.gitlab-ci.yml](./templates/Node-Test.gitlab-ci.yml)
- [Node.gitlab-ci.yml](./templates/Node.gitlab-ci.yml)
- [Python.gitlab-ci.yml](./templates/Python.gitlab-ci.yml)
- [Skopeo.gitlab-ci.yml](./templates/Skopeo.gitlab-ci.yml)
- [Stages.gitlab-ci.yml](./templates/Stages.gitlab-ci.yml)
